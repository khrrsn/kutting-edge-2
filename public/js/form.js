(function($) {
    
    var defaults = {
        className: 'fancyselect',
        openClassName: 'open',
        titleEl: 'h3'
    };
    var settings;
 
    $.fn.fancyselect = function(settings) {
        settings = $.extend(defaults, settings);
        
        this.each(function() {
            var $this = $(this).hide();

            $this.after('<div class="' + settings.className + '"><' + settings.titleEl + '><span></span></' + settings.titleEl + '><span class="arrow"></span><ul></ul></div>');

            var $dropdown = $this.next('.' + settings.className);
            var $list = $dropdown.find('ul');
            var $selected = $this.find('option[selected]');
            
            var titleText = '';
            if($selected.length) {
                titleText = $selected.val();
            } else {
                titleText = $this.find('option:first-child').text()
            }
            var $title = $dropdown.find(settings.titleEl + ' span').text(titleText);

            $this.find('option').each(function() {
                $list.append('<li><a href="#' + $(this).val() + '">' + $(this).text() + '</a></li>');
            });

            var $listitems = $list.find('li').hide();
            
            function close() {
                $(document).unbind('mouseup.fancyselect');
                $listitems.slideUp(100, function() {
                    $dropdown.removeClass(settings.openClassName);
                    isOpen = false;
                });
            }

            var isOpen = false;
            $dropdown.bind('click', function() {
                if(!isOpen) {
                    $dropdown.addClass(settings.openClassName);
                    $listitems.slideDown(200, function() {
                        isOpen = true;
                    });
                    $(document).bind('mouseup.fancyselect', function() {
                        close();
                    });
                } else {
                    close();
                }
            });

            $list.find('a').bind('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $this.val($(this).attr('href').replace('#', '')).change();
                $title.text($(this).text());
                close();
            });
        });
    }
    
})(jQuery);