$(function() {
	$('form').validate();

	$('ul.optionData').hide();

	$('.checkbox').click(function() {
		$(this).next().slideToggle();
	});

	if (!Modernizer.inputtypes.date) {
		$('input[type="date"]').datepicker();
	}

});