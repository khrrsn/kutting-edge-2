$(function() {
	$('table').dataTable();
	$('table.products').dataTable();
	$('table.options').dataTable();
	$('.dataTables_filter').attr('id', 'search');
});