function selectAll( obj_arr ){

	var obj_sel;
	for ( var i = 0; i < obj_arr.length; i++ ){
	
		obj_sel = document.getElementById( obj_arr[i] );
		
		for( var j = 0; j < obj_sel.options.length; j++ ){
			obj_sel.options[j].selected = true;
		}
		
	}
	
}

function move( id_1, id_2 ){
        
        //the box where the value will come from
        var opt_obj = document.getElementById( id_1 );
        
        //the box where the value will be locationd
        var sel_obj = document.getElementById( id_2 );
        
        for ( var i = 0; i < opt_obj.options.length; i++ ){ //loop to check for multiple selections
                
                if ( opt_obj.options[i].selected == true ){ //check if the option was selected               
                        //value to be transfered
                        var selected_text = opt_obj.options[i].text;
                        var selected_value = opt_obj.options[i].value;
                        
                        //remove from opt
                        opt_obj.remove( i );
                        
                        //decrease value of i since an option was removed
                        //therefore the opt_obj.options.length will also decrease
                        i--;
                        
                        //process to sel
                        var new_option_index = sel_obj.options.length;
                        sel_obj.options[new_option_index] = new Option( selected_text, selected_value );
                        
                }
                
        }
}

$(function() {
        $('form.customer').validate();
        if (!Modernizr.inputtypes.date) {
                $('input[type="date"]').datepicker();
        }
});