// Include merchant logo
// Include add to cart graphic
// Include form name and number of options by passing to createForm. ie: createForm("theFormer",2)

// loadItem() prepares a psigate form template for that item, executed when clicking buy now button
// 
// 

var theItem = '';
var numOptions = 0;
var shipByProvince = 0;

var lists = new Array();

// First set of text and values
lists['Canada']    = new Array();
lists['Canada'][0] = new Array(
'Alberta',
'British Columbia',
'Manitoba',
'New Brunswick',
'Newfoundland',
'NWT',
'Nova Scotia',
'Nunavut',
'Ontario',
'PEI',
'Qu�bec',
'Saskatchewan',
'Yukon Territory'
);

// Second set of text and values
lists['USA']    = new Array();
lists['USA'][0] = new Array(
'Alabama',
'Arizona',
'Arkansas',
'California',
'Colorado',
'Connecticut',
'Delaware',
'District of Columbia',
'Florida',
'Georgia',
'Hawaii',
'Idaho',
'Illinois',
'Indiana',
'Iowa',
'Kansas',
'Kentucky',
'Louisiana',
'Maine',
'Maryland',
'Massachusetts',
'Michigan',
'Minnesota',
'Mississippi',
'Missouri',
'Montana',
'Nebraska',
'Nevada',
'New Hampshire',
'New Jersey',
'New Mexico',
'New York',
'North Carolina',
'North Dakota',
'Ohio',
'Oklahoma',
'Oregon',
'Pennsylvania',
'Puerto Rico',
'Rhode Island',
'South Carolina',
'South Dakota',
'Tennessee',
'Texas',
'Utah',
'Vermont',
'Virginia',
'Virgin Islands',
'Washington',
'West Virginia',
'Wisconsin',
'Wyoming'
);

// Third set of text and values
lists['in']    = new Array();
lists['in'][0] = new Array(
'International'
);

function emptyList( box ) {
	// Set each option to null thus removing it
	while ( box.options.length ) box.options[0] = null;
}

// This function assigns new drop down options to the given
// drop down box from the list of lists specified

function fillList( box, arr ) {
	// arr[0] holds the display text
	// arr[1] are the values

	for ( i = 0; i < arr[0].length; i++ ) {

		// Create a new drop down option with the
		// display text and value from arr

		option = new Option( arr[0][i], arr[0][i]);

		// Add to the end of the existing options

		box.options[box.length] = option;
	}

	// Preselect option 0

	box.selectedIndex=0;
}

function setProvince()
{
	document.forms['innerForm'].Sprovince.value = document.forms['innerForm'].Bprovince.value;
	doUpdate();
}

function setCountry()
{
	document.forms['innerForm'].Scountry.value = document.forms['innerForm'].Bcountry.value;
	doUpdate();
}

function setName()
{
	document.forms['innerForm'].Sname.value = document.forms['innerForm'].Bname.value;
	doUpdate();
	//document.forms['outerForm'].Bname.value = document.forms['innerForm'].Bname.value;
	//document.forms['outerForm'].Sname.value = document.forms['innerForm'].Sname.value
}

function setAddress()
{
	document.forms['innerForm'].Saddress1.value = document.forms['innerForm'].Baddress1.value;
	
}

function setCity()
{
	document.forms['innerForm'].Scity.value = document.forms['innerForm'].Bcity.value;
	
}

function setPostalCode()
{
	document.forms['innerForm'].Spostalcode.value = document.forms['innerForm'].Bpostalcode.value;
	
}

function setPhone()
{
	
}

function setEmail()
{
	
}

// This function performs a drop down list option change by first
// emptying the existing option list and then assigning a new set
function changeList( box ) {

// Flush variable used to calculate international shipping used later on in priceonPage
intShipping=0;
setCountry();

if (document.forms['innerForm'].Bcountry.value=="Canada" || document.forms['innerForm'].Bcountry.value=="USA") {

	document.getElementById('innerProvince').innerHTML='<select name=\"Bprovince\" style=\"width:133px;\" onChange=\"setProvince();\">';

// Isolate the appropriate list by using the value
// of the currently selected option

	list = lists[box.options[box.selectedIndex].value];

	// Next empty the fill list

	emptyList( box.form.Bprovince );

	// Then assign the new list values

	fillList( box.form.Bprovince, list );
	setProvince();


} else {

	document.getElementById('innerProvince').innerHTML='<input type=text name=\"Bprovince\" style=\"width:130px;\" onChange=\"setProvince();\"> ';
	document.forms['innerForm'].Sprovince.value="";

}

doUpdate();

}

// Outputs string to declare options in form sent to psigate. Argument is the number of options available for the item
function generateOptions()
{
	var outputString = "";
	
	// create output String
	// loops for number of options chosen by merchant
	if ( numOptions > 0 )
	{
		for ( i = 1; i <= numOptions; i++ )
		{
			outputString += '<input class="option" type="hidden" name="OptionName010'+i+'" value="">';
			outputString += '<input class="option" type="hidden" name="OptionValue010'+i+'" value="">';	
		}
	}
	
	return outputString;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Begin createForms /////////////////////////////////////////////////////////////////////////


function loadItem(item,nOptions,sByProvince)
{
	loadedDiv='';
	if (document.forms[item].testMode.value=="0") { theFormAction = "https://checkout.psigate.com/HTMLPost/HTMLMessenger"; } else { theFormAction="https://devcheckout.psigate.com/HTMLPost/HTMLMessenger"; }
	theItem = item;
	numOptions = nOptions;
	shipByProvince = sByProvince;
	loadedDiv='<div style="background:#ffffff;color:#222222;border:1px solid #222222; padding-top:5px;padding-bottom:0px;padding-left:10px;padding-right:10px;font-family:arial;">' +
		'<form name="innerForm" onSubmit="return checkForm()" action="'+theFormAction+'" method="post">' +
		'<input type="hidden" name="PaymentType" VALUE="CC">' +
		'<input type="hidden" name="MerchantID" value="">' +
		'<input type="hidden" name="Description01" value="">' +
		'<input type="hidden" name="Quantity01" value="">' +
		generateOptions() +
		'<input type="hidden" name="Price01" value="">' +
		'<input type="hidden" name="OrderID" value="">' +
		'<input type="hidden" name="CardAction" value="">' +
		'<input type="hidden" name="Tax1" value="">' +
		'<input type="hidden" name="ItemID01" value="EasyCheckout-01">' +
		'<input type="hidden" name="ShippingTotal" value="">' +
		'<table border=0 cellpadding=3 cellspacing=0>' +
		'<tr><Td></td><td><b style="font-size:14px";>Billing Address</b></td><td width=5></td><td><b style="font-size:14px;">Shipping Address</b></td></tr>' +
		'<tr><Td>Name:</td> <td><input type="text" name="Bname" style="width:130px;" onChange="setName();"></td><td width=5></td><td><input type="text" name="Sname" style="width:130px;"></td></tr>' +
		'<tr><Td>Country:</td><td>' +
		'      <select name="Bcountry" style="width:133px;" onchange="changeList(this);">' +
		'      <option value="Canada">Canada' +
		'      <option value="USA">United States' +
		'      <option value="United Kingdom">United Kingdom' +
		'      <option value="Australia">Australia' +
		'      <option value="New Zealand">New Zealand' +
		'      <option value="International">None of the Above' +
		'      </select>' +
		'</td><td width=5></td><td><input type="text" name="Scountry" style="width:130px;" value="Canada"></td></tr>' +
		'<tr><Td>Street Address: </td><td><input type="text" name="Baddress1" style="width:130px;" onChange="setAddress();"></td><td width=5></td><td><input type="text" name="Saddress1" style="width:130px;"></td></tr>' +
		'<tr><Td>City:</td><td> <input type="text" name="Bcity" style="width:130px;" onChange="setCity();"></td><td width=5></td><td><input type="text" name="Scity" style="width:130px;"></td></tr>' +
		'<tr><Td>Prov/State</td><td>' +
		'      <span id="innerProvince">' +
		'      <select name="Bprovince" style="width:133px;" onChange="setProvince();">' +
		'      <option value="Alberta">Alberta' +
		'      <option value="British Columbia">British Columbia' +
		'      <option value="Manitoba">Manitoba' +
		'      <option value="New Brunswick">New Brunswick' +
		'      <option value="Newfoundland">Newfoundland' +
		'      <option value="NWT">NWT' +
		'      <option value="Nova Scotia">Nova Scotia' +
		'      <option value="Nunavut">Nunavut' +
		'      <option value="Ontario">Ontario' +
		'      <option value="PEI">PEI' +
		'      <option value="Qu�bec">Qu�bec' +
		'      <option value="Saskatchewan">Saskatchewan' +
		'      <option value="Yukon Territory">Yukon Territory' +
		'      </select>' +
		'      </span>' +
		'</td><td width=5></td><td><input type="text" name="Sprovince" style="width:130px;" value="Alberta"></td></tr>' +
		'<tr><Td>Postal/Zip: </td><td><input type="text" name="Bpostalcode" style="width:130px;" onChange="setPostalCode();"></td><td width=5></td><td><input type="text" name="Spostalcode" style="width:130px;"></td></tr>' +
		'<tr><Td>Telephone: </td><td><input type="text" name="Phone" style="width:130px;" onChange="setPhone();"></td><td width=5></td><td></td></tr>' +
		'<tr><Td>E-mail:</td><td> <input type="text" name="Email" style="width:130px;" onChange="setEmail();"></td><td width=5></td><td></td></tr>' +
		'</table>' +
		'<BR>' +
		'<table style="margin-bottom:15px;" border=0 width=420 cellpadding=3 cellspacing=0>' +
		'<tr><td width=210 valign=top>' +
		'<b>Your Order Details:</b><br><br>' +
		'<span id="description"></span> <br>' +
		'<span id="theOptions"></span><br>' +
		'Subtotal:   $<span id="theSubTotal">0.00</span> <br>' +
		'Shipping: $<span id="spanShipping"></span> <br>' +
		'Tax: $<span id="taxTotal"></span> <br>' +
		'</td>' +
		'<td width=210 valign=bottom align=right>' +
		'<table border=0 cellpadding=0 cellspacing=0>' +
		'<tr><td valign=bottom style="font-size:10px;font-family:arial;">EasyCheckout Powered By</td><td><a href="http://www.merchant-accounts.ca" target="_blank"><img src="merchantAccountsLogo.gif" border=0></a></td></tr>' +
		'<tr><td colspan=2><a href="http://www.merchant-accounts.ca" target="_blank" style="font-size:16px;color:#6ea4f0;font-family:arial;font-weight:bold;text-decoration:none;">Merchant Accounts.ca</a></td></tr>'+
		'</table>'+
		'<br>'+
		'<div style="font-size:15px;font-weight:bold;">Order Total: $<span id="orderTotal" style="font-size:20px;"></span>&nbsp;  </div><br>' +
		'<input style="margin-bottom:15px;width:200px;" type="submit" value="Submit Your Order">' +
		'</td>' +
		'</tr>' +
		'</table>' +
		'</form>' +
		'</div>';
	
	return loadedDiv;
}

// set psigate options variables and display options in tinybox
function setOptions()
{
	var theOptions='';	// string to display options in tinybox checkout
	var optionsList = new Array();	//document.forms['innerForm'].getElementsByClassName("option"); doesn't work in IE
	var referenceList = new Array();	//document.forms[theItem].getElementsByClassName("option"); doesn't work in IE
	var temp = document.forms['innerForm'].getElementsByTagName("input");
	var y = 0;
	var x = 0;
	
	if ( numOptions > 0 )
	{
		//get option elements from psigate form
		for (i = 0; i < temp.length; i++)
		{
			if (temp[i].className == "option")
			{
				optionsList[y] = temp[i];
				y++;
			}	
		}
		
		//get option elements from item form
		var y = 0;
		var temp = document.forms[theItem].getElementsByTagName("select");
		for (i = 0; i < temp.length; i++)
		{
			if (temp[i].className == "option")
			{
				referenceList[y] = temp[i];
				y++;
			}	
		}
		
		//set psigate form, build options display string for checkout form
		for (i = 0; i < numOptions; i++ )
		{			
			optionsList[x].value = referenceList[i].title; // set option name for psigate form
			theOptions += optionsList[x].value+ ": ";
			x++;
			optionsList[x].value = referenceList[i].value;	// set option value for psigate form
			theOptions += optionsList[x].value + "<br>";	
			x++;
		}				
	}
	document.getElementById('theOptions').innerHTML=theOptions; //set options summary checkout form
}
//  End createForm
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////
// Begin doUpdate /////////////////////////////////////////////////////////////

function doUpdate() {

var subTotal="";
shipCost=0;
provTaxCost="";
countryTaxCost="";
var finalTotal="";
var productDescription="";
var orderSummary ="";
//  Tax Cost
var taxTable=document.forms[theItem].taxRates.value.split("|")
var applicableTaxRate = "0.00";
var taxTotal ="0.00";

setOptions();

productDescription = document.forms[theItem].quantity.value + ' - ' + document.forms[theItem].productDescription.value;

subTotal = (document.forms[theItem].quantity.value * document.forms[theItem].itemCost.value)

var provinces = new Array(13)
provinces[0]='Alberta'
provinces[1]='British Columbia'
provinces[2]='Manitoba'
provinces[3]='New Brunswick'
provinces[4]='Newfoundland'
provinces[5]='NWT'
provinces[6]='Nova Scotia'
provinces[7]='Nunavut'
provinces[8]='Ontario'
provinces[9]='PEI'
provinces[10]='Qu�bec'
provinces[11]='Saskatchewan'
provinces[12]='Yukon Territory'

// Shipping Cost in Canada
if (document.forms['innerForm'].Bcountry.value == "Canada")
{
	if (shipByProvince==1)
	{
		var shippingTable=document.forms[theItem].shippingCostCanada.value.split("|")
		var loopCheck2=0;
		while (loopCheck2<14)
		{
			if (provinces[loopCheck2] == document.forms['innerForm'].Bprovince.value)
			{
				if (document.forms[theItem].shippingCalculation.value == "flatRate")
				{
					shipCost = shippingTable[loopCheck2];
				}
				else
				{
					shipCost = (shippingTable[loopCheck2] * document.forms[theItem].quantity.value)
				}
			}
			loopCheck2++;
		}
	}
	else
	{
		if (document.forms[theItem].shippingCalculation.value == "flatRate") {  shipCost = document.forms[theItem].shippingCostCanada.value; } else { shipCost = (document.forms[theItem].shippingCostCanada.value * document.forms[theItem].quantity.value); }
	}
	
	// if drop down province == loopcheck province, then charge the corresponding sales tax.
	var loopCheck=0;
	while (loopCheck<14) {
		  if (provinces[loopCheck] == document.forms['innerForm'].Bprovince.value) { // Provinces matched, lets charge some tax
			   if (taxTable[loopCheck] > 0) { applicableTaxRate = (taxTable[loopCheck] * .01);    }
			   taxTotal = subTotal * applicableTaxRate;
			   taxTotal = Math.round(((taxTotal/1))*100)/100;
			   if (taxTotal.toFixed) { taxTotal = taxTotal.toFixed(2); }    // sets it to two decimal places.
		  }

	loopCheck ++;
	}
}

// Shipping Cost in USA
if (document.forms['innerForm'].Bcountry.value == "USA") { if (document.forms[theItem].shippingCalculation.value == "flatRate") {  shipCost = document.forms[theItem].shippingCostUSA.value; } else { shipCost = (document.forms[theItem].shippingCostUSA.value * document.forms[theItem].quantity.value); } }

// Shipping Cost International
if (document.forms['innerForm'].Bcountry.value != "Canada" && document.forms['innerForm'].Bcountry.value != "USA") { if (document.forms[theItem].shippingCalculation.value == "flatRate") {  shipCost = document.forms[theItem].shippingCostInternational.value; } else { shipCost = (document.forms[theItem].shippingCostInternational.value * document.forms[theItem].quantity.value); } }


shipCost = Math.round(((shipCost/1))*100)/100;
if (shipCost.toFixed) { shipCost = shipCost.toFixed(2); }    // sets it to two decimal places.

finalTotal = Number(subTotal) + Number(shipCost) + Number(taxTotal);
finalTotal = Math.round(((finalTotal/1))*100)/100;

// sets it to two decimal places.  The if statement just checks to make sure the browser supports the twoFixed command.  (Javascript 1.5 and above)
if (finalTotal.toFixed) { finalTotal = finalTotal.toFixed(2); }


// Display the calculated totals on the orderform
document.getElementById('theSubTotal').innerHTML=subTotal;
document.getElementById('spanShipping').innerHTML=shipCost;
document.getElementById('taxTotal').innerHTML=taxTotal;
document.getElementById('description').innerHTML=productDescription;
document.getElementById('orderTotal').innerHTML=finalTotal;


// Update the form data for sending transaction to gateway
// document.forms['innerForm'].SubTotal.value = finalTotal;  HIDE the subtotal because the gateway adds it to the price of the product alreayd declared
document.forms['innerForm'].CardAction.value = document.forms[theItem].transactionMode.value

// If set to testmode, we must use a test StoreKey, otherwise use the real StoreKey
if (document.forms[theItem].testMode.value=="0") { document.forms['innerForm'].MerchantID.value = document.forms[theItem].installation.value; } else { document.forms['innerForm'].MerchantID.value = 'psigatecapturescard001010'; }

document.forms['innerForm'].Description01.value = document.forms[theItem].productDescription.value;
document.forms['innerForm'].Price01.value = document.forms[theItem].itemCost.value;
document.forms['innerForm'].Quantity01.value = document.forms[theItem].quantity.value;
document.forms['innerForm'].Tax1.value = taxTotal;
document.forms['innerForm'].ShippingTotal.value = shipCost;

var timestamp = Number(new Date()); // current time as number
document.forms['innerForm'].OrderID.value = timestamp;

if (document.forms[theItem].shippingCalculation.value == "flatRate") {  shipCost = document.forms[theItem].shippingCostCanada.value; } else { shipCost = (document.forms[theItem].shippingCostCanada.value * document.forms[theItem].quantity.value); }

}
// End doUpdate /////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////


function checkForm() {

doUpdate();
//number of input fields before error checking begins
var n = (10+numOptions*2);
var elem = document.forms['innerForm'].elements;
var missing=false;

//loop through elements of form

for ( i=n; i<elem.length; i++ )
{
	if (elem[i].value=='')
	{
		missing=true;
		elem[i].style.backgroundColor="#FFCD8C";
	}
	else
	{
		elem[i].style.backgroundColor="#FFFFFF";
	}
}

if (missing)
{
	alert('Without your contact information, we cannot ship your order! Required fields are highlighted.')
	return false;
}
else
{
	return true;
}
/**
if (document.forms['innerForm'].Email.value=='') { (missing=1); }

 if (missing==1) {
 alert ('Your email address is required.  \n\n If you dont include your contact information\, we cant ship your order!');
 document.forms['innerForm'].Email.focus();
 return false
 }
*/

}