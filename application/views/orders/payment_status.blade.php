<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Kutting Edge: Payment Status</title>
	{{ HTML::style('css/global.css') }}
	{{ HTML::style('css/form.css') }}
	<link rel="shortcut icon" href="<?php echo URL::to('favicon.ico'); ?>">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
	{{ HTML::script('js/jquery.validate.min.js') }}
	<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<div id="form-view">
		<div class="head">
			<h1>Kutting Edge</h1>
			<h2>Payment Status</h2>
			<div class="right">
			</div>
		</div>
<style type="text/css">
	th { font-weight: bold; }
</style>
<?php if (Input::get('Approved') == 'DECLINED') : ?>
<center>
	<h2>I'm sorry, Your payment has been declined. Here is some more information:</h2>
</center>
<br><br>
<?= serialize(Input::all()) ?>
			<table width="60%" style="margin:auto">
				<thead>
					<tr>
						<th>Title</th>
						<th>Value</th>
					</tr>
				</thead>
				<tr>
					<td>Transaction Time</td>
					<td>{{ Input::get('TransTime') }}</td>
				</tr>
				<tr>
					<td>Order ID</td>
					<td>{{ Input::get('OrderID') }}</td>
				</tr>
				<tr>
					<td>Error Message</td>
					<td>{{ Input::get('ErrMsg') }}</td>
				</tr>
				<tr>
					<td>Payment Type</td>
					<td>{{ Input::get('PaymentType') }}</td>
				</tr>
				<tr>
					<td>Card Number</td>
					<td>{{ Input::get('CardNumber') }}</td>
				</tr>
				<tr>
					<td>Price</td>
					<td>${{ Input::get('FullTotal') }}</td>
				</tr>

			</table>
			<br>
	<center>
		<p>Please <strong><a style="color:#000" href="{{ URL::to('orders/payment/') }}{{ Input::get('CustomerRefNo'); }}">Go Back</a></strong> and try again.</p>
	</center>
	<?php elseif ( Input::get('Approved' == 'APPROVED') ) : ?>

			<?php
				$customer = CartSession::find(Input::get('CustomerRefNo'));
				$payment = Payment::create(
					array(
						'payment' => Input::get('FullTotal'),
						'customer' => Input::get('CustomerRefNo'),
						'client' => $customer->client,
						'info' => serialize(Input::all()),
					)
				);
				if ( $payment ) {
					return Redirect::to('orders/thanks/' . Input::get('CustomerRefNo'));
				}
			?>

		<?php endif; ?>
@include('orders/_partials/footer')