<?php $org = Organization::find($client->organization); ?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Kutting Edge: {{ $client->customername }} (Order Form)</title>
	{{ HTML::style('css/global.css') }}
	{{ HTML::style('css/form.css') }}
	<link rel="shortcut icon" href="<?php echo URL::to('favicon.ico'); ?>">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
	{{ HTML::script('js/jquery.validate.min.js') }}
	<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<div id="form-view">
		<div class="head">
			<h1>Kutting Edge</h1>
			<h2>Order Form - {{ $client->customername }}</h2>
			<div class="right">
			</div>
		</div>
		<?php if ( URL::current() == URL::to('orders/confirm') ) {}
		  elseif ( URL::current() == URL::to('orders/payment') ) {}
		else { ?>
		<div class="breadcrumb">
			<ul>
				<li style="z-index: 100"<?php if( URL::current() == URL::to('orders/') . $client->id ) echo ' class="active"'; ?>>
					<span class="number">1</span>
					<span class="step">Step One</span>
					<span class="desc">Personal Information</span>
				</li>
				<li style="z-index: 75"<?php if( URL::current() == URL::to('orders/items') ) echo ' class="active"'; ?>>
					<span class="number">2</span>
					<span class="step">Step Two</span>
					<span class="desc">Item Selection</span>
				</li>
				<li style="z-index: 50"<?php if( URL::current() == URL::to('orders/finalization') ) echo ' class="active"'; ?>>
					<span class="number">3</span>
					<span class="step">Step Three</span>
					<span class="desc">Finalization</span>
				</li>
			</ul>
		</div>
	<?php } ?>
