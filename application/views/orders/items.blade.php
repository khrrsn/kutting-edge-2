@include('orders/_partials/header')
	<?php
		$productsArray = unserialize($client->products);
		if ( is_array($productsArray)) :
		$productarray = Product::where_in('id', $productsArray)->get();
		else:
			$productarray = Product::find($productsArray);
		endif;
	?>
	<div id="orderItems">
		<?php if ( is_array($productsArray) ) {
		$arrayCheck = true;
	} else {
		$arrayCheck = false;
	} ?>
	<?php if ($arrayCheck == true) : ?>
		@foreach ( $productarray as $product )
		{{ Form::open('orders/finalization', 'post', array( 'id' => $product->id, 'class' => 'item')) }}
		<input type="hidden" name="product_id" value="{{$product->id}}">
			<section id="product">
				<div class="left">
		<?php
			$thumb = explode('/', $product->file);
			$thumb = $thumb[0] . '/thumb_' . $thumb[1];
		?>
		<img src="{{ URL::base() }}/{{ $thumb }}" width="156" height="156" alt="{{ $product->title }}">
				</div>
				<div class="left" id="info">
					<div class="left">
						<h2>{{ $product->title }}</h2>
						<table>
							<tr>
								<td>
									<span class="colours">
										<label for="colours">Colours</label>
										<?php $colors = unserialize($client->colours); $i = 0; ?>
										@foreach ( $colors as $color )
											<?php $i++;  $color = Color::find($color); ?>
											<span class="box" style="background:{{ $color->hex }}"><input type="radio" name="color" value="{{ $color->id }}"<?php if ( $i == 1) echo ' checked'; ?>></span>
										@endforeach
									</span>
								</td>
								<?php $sizes = unserialize($product->sizes); ?>
								<td style="vertical-align: top; padding-left:20px">
									<span class="sizes">
										<label for="sizes">Sizes</label>
										<select name="sizes">
											@foreach( $sizes as $size )
												<option value="{{ $size }}">{{ $size }}</option>
											@endforeach
										</select>
									</span>
								</td>
						</table>
						<?php $optionsArray = unserialize($product->options); ?>
								<?php if ( $optionsArray != '') : ?>
								<span class="custom">
									<label for="co">Customization Options</label>
									<?php $opts = Options::where_in('id', $optionsArray)->get(); ?>
									<ul>
									<?php $int = 0; ?>
									@foreach ( $opts as $opt)
										<?php $int++; ?>
										<li><input class="checkbox" type="checkbox" name="options[]" value="{{ $opt->id }}">{{ $opt->title }} (+{{ $opt->price}})
											<ul class="optionData">
												<li>
													<span class="desc">{{ $opt->description }}</span>
													<?php if ( $opt->form == '1') : ?>
														<textarea name="formOption[{{ $opt->id}}]"></textarea>
													<?php elseif ( $opt->form == '2' ) : ?>
														<input type="text" name="formOption[{{ $opt->id}}]">
													<?php endif; ?>
												</li>
											</ul>
										</li>	
									@endforeach
									</ul>
								</span>
						<?php endif; ?>
					</div>
					<div id="price" class="right">
						Item Price: {{ $product->price }}
						<br>
						<span class="quantity"><label class="quantity" for="quanity">QTY:</label><input type="number" min="1" value="1" class="quantity required" name="quantity" required></span>
						<br>
						<button class="cart" onclick="addToCart({{ $product->id }}); return false;">Add to Cart</button>
					</div>
					<span class="inYourCart">&#x2713; In Your Cart!</span>
				</div>
			</section>
			{{ Form::close() }}
		@endforeach
		<?php elseif ( $arrayCheck == false ) : ?>
		<?php $product = $productarray; ?>
		{{ Form::open('orders/finalization', 'post', array( 'id' => $product->id, 'class' => 'item')) }}
		<input type="hidden" name="product_id" value="{{$product->id}}">
			<section id="product">
				<div class="left">
		<?php
			$thumb = explode('/', $product->file);
			$thumb = $thumb[0] . '/thumb_' . $thumb[1];
		?>
		<img src="{{ URL::base() }}/{{ $thumb }}" width="156" height="156" alt="{{ $product->title }}">
				</div>
				<div class="left" id="info">
					<div class="left">
						<h2>{{ $product->title }}</h2>
						<table>
							<tr>
								<td>
									<span class="colours">
										<label for="colours">Colours</label>
										<?php $colors = unserialize($client->colours); $i = 0; ?>
										@foreach ( $colors as $color )
											<?php $i++;  $color = Color::find($color); ?>
											<span class="box" style="background:{{ $color->hex }}"><input type="radio" name="color" value="{{ $color->id }}"<?php if ( $i == 1) echo ' checked'; ?>></span>
										@endforeach
									</span>
								</td>
								<?php $sizes = unserialize($product->sizes); ?>
								<?php if ($sizes == 0 ) {} else { ?>
								<td style="vertical-align: top; padding-left:20px">
									<span class="sizes">
										<label for="sizes">Sizes</label>
										<select name="sizes">
											@foreach( $sizes as $size )
												<option value="{{ $size }}">{{ $size }}</option>
											@endforeach
										</select>
									</span>
								</td>
							<?php } ?>
						</table>
						<?php $optionsArray = unserialize($product->options); ?>
								<?php if ( $optionsArray != '') : ?>
								<span class="custom">
									<label for="co">Customization Options</label>
									<?php $opts = Options::where_in('id', $optionsArray)->get(); ?>
									<ul>
									<?php $int = 0; ?>
									@foreach ( $opts as $opt)
										<?php $int++; ?>
										<li><input class="checkbox" type="checkbox" name="options[]" value="{{ $opt->id }}">{{ $opt->title }} (+{{ $opt->price}})
											<ul class="optionData">
												<li>
													<span class="desc">{{ $opt->description }}</span>
													<?php if ( $opt->form == '1') : ?>
														<textarea name="formOption[{{ $opt->id}}]"></textarea>
													<?php elseif ( $opt->form == '2' ) : ?>
														<input type="text" name="formOption[{{ $opt->id}}]">
													<?php endif; ?>
												</li>
											</ul>
										</li>	
									@endforeach
									</ul>
								</span>
						<?php endif; ?>
					</div>
					<div id="price" class="right">
						Item Price: {{ $product->price }}
						<br>
						<span class="quantity"><label class="quantity" for="quanity">QTY:</label>
							<input type="number" name="quantity" class="quantity" min="0" max="10" step="3" value="6" />
						</span>
						<br>
						<button class="cart" onclick="addToCart({{ $product->id }}); return false;">Add to Cart</button>
					</div>
					<span class="inyourCart">&#x2713; In Your Cart!</span>
				</div>
			</section>
			{{ Form::close() }}
			<?php endif; ?>
		<div class="right">
				{{ Form::open('orders/finalization', 'post') }}
					<input type="hidden" name="client" value="{{ $client->id }}">
					<input type="hidden" name="session_id" value="{{$session_id}}">
					<button onclick="window.location='{{ URL::to('orders/cancel/') }}{{$session_id}}'" type="button">Cancel Order</button></a>
					<button type="submit">Finalization &raquo;</button>
				{{ Form::close() }}
			</div>
	</div>
	<script type="text/javascript">
		$('.inYourCart').hide();
		$('form').validate();
		function addToCart(product_id) {
			var dataString = $('form#' + product_id).serializeArray();
			var request = $.ajax({
				type: "post",
				url: "{{ URL::to('orders/add/') }}{{ $session_id }}",
				data: dataString
			});

			request.done(function(msg) {
				$('form#' + product_id).fadeOut(100).fadeIn(100);
				$('form#' + product_id).find('.inYourCart').fadeIn(100);
			});
			return false;
		}
	</script>
@include('orders/_partials/footer')