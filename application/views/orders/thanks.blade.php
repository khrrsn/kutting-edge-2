@include('orders/_partials/header')
<h2 style="margin-top: 20px;text-align:center">Thank you for placing your order, please check your email for an order confirmation email.</h2>
<h2 style="font-size: 15px; text-align:center; margin-top: 30px">Your order will be processed in a timely fashion.</h2>
@include('orders/_partials/footer')