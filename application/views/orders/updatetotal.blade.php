<?php $carted = CartProducts::where('shopper', '=', $session_id)->get(); $sum = 0; ?>
<?php $session = CartSession::find($session_id); ?>
<?php $client = Client::find($session->client); ?>
@foreach ( $carted as $cart )
	<?php $product = unserialize($cart->product); ?>
	<?php $item = Product::find($product['product_id']); ?>
	<?php $price = explode('$', $item->price) ?>
	<?php $priced = $price[1] * $product['quantity']; ?>
	<?php $sum += $priced; ?>
	<?php if (isset($product['options'])) { ?>
	@foreach( $product['options'] as $opt )
		<?php $option = Options::find($opt);
			$optPrice = explode('$', $option->price);
			$sum += $optPrice[1];
		?>
	@endforeach
	<?php } ?>
@endforeach

<table width="160">
	<tr>
		<th><b>Subtotal</b></th>
		<th>${{ $sum }}</th>
	</tr>
	<tr>
		<td><b>Taxes (13%)</b></td>
		<td>$<?php $tax = $sum * .13; $tax = round($tax, 2); $tax = money_format('%i', $tax); ?>{{ $tax }}</td>
	</tr>
	<?php if ( $client->shipping > '' ) : ?>
	<?php if ( $num == '2' ) : ?>
		<tr>
			<td><b>Shipping Cost</b></td>
			<td>{{ $client->shipping }}</td>
			<?php $shipping = explode("$", $client->shipping) ?>
			<?php $shipping = $shipping[1]; ?>
		</tr>
	<?php else: $shipping = 0; endif; ?>
	<?php else: $shipping = 0; endif; ?>
	<?php  ?>
	<tr>
		<td><b>Total</b></td>
		<?php $total = $tax + $sum + $shipping; ?>
		<td>${{ money_format('%i', $total) }}</td>
	</tr>
			
</table>