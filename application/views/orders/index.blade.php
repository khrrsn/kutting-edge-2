@include('orders/_partials/header')
<?php 
	$fromUser = new DateTime(date("Y-m-d H:i:s"));
	if (
		$fromUser > new DateTime($client->order_start . '00:00:00') &&
		$fromUser < new DateTime($client->order_finish . '23:59:59'))
	{
?>
		<noscript><h2><span class="error"><center>Please enable JS, or else you will not be able to order any items.</center></span></h2></noscript>
		{{ Form::open('orders/items/', 'post') }}
			<?php $org = Organization::find($client->organization); ?>
			<?php $req = unserialize($org->required); ?>
			<input type="hidden" value="{{$client->id}}" name="id">
			<div class="left">
				<?php if (in_array('firstName', $req)) : ?>
					<label for="firstName">First Name</label>
					<input type="text" tabindex="1" class="required" name="firstName">
				<?php endif; ?>
				<?php if (in_array('address', $req)) : ?>
					<label for="address-1">Shipping Address</label>
					<input type="text" name="address-1" class="required">
					<input type="text" name="address-2">
					<input type="text" name="address-3">
				<?php endif; ?>
				<?php if (in_array('teacher', $req)) : ?>
					<label for="homeroom">Homeroom Teacher</label>
					<input type="text" name="homeroom" class="required">
				<?php endif; ?>
			</div>
			<div class="left" id="right">
				
				<div class="group">
					<span class="small">
						<?php if (in_array('middle', $req)) : ?>
							<label for="middle">Middle</label>
							<input type="text" name="middle" class="required">
						<?php endif; ?>
					
					</span>
					
						<?php if (in_array('lastName', $req)) : ?>
							<label for="lastName">Last Name</label>
							<input tabindex="2" type="text" name="lastName" class="required">
						<?php endif; ?>
				</div>
				<?php if (in_array('phoneNumber', $req)) : ?>
				<label for="phoneNumber">Phone Number</label>
				<input type="text" name="phoneNumber" class="required">
				<?php endif; ?>

				<?php if (in_array('email', $req)) : ?>
				<label for="email">Email Address</label>
				<input type="text" name="email" class="required">
				<?php endif; ?>

				<span class="small">
					<?php if (in_array('room', $req)) : ?>
					<label for="room">Room</label>
					<input type="text" name="room" class="required">
					<?php endif; ?>
				</span>

				<?php if ( $org->custom > '' ) : $i = 0; ?>
					<?php $cf = unserialize($org->custom); ?>
					@foreach( $cf as $custom )
					<?php $i++; ?>
						<?php if ( $custom > '' ) : ?>
							<label for="{{$i}}_custom">{{ $custom }}</label>
							<input type="text" name="{{$i}}_custom">
						<?php endif; ?>
					@endforeach
				<?php endif; ?>

			</div>
			<div class="clear"></div>
			<br><br>
			<div class="right">
				<button type="submit">Continue &raquo;</button>
			</div>
		{{ Form::close() }}
		<?php
			} else {
				if ( $fromUser < new DateTime($client->order_start . '00:00:00') ) {
					echo "<h2 class='error'>I'm sorry this order form has not started yet.</h2>";
				} elseif ( $fromUser > new DateTime($client->order_finish . '23:59:59') ) {
					echo "<h2 class='error'>I'm sorry this order form has already ended.</h2>";
				}
			}
		?>
		<div class="clear"></div>

@include('orders/_partials/footer')