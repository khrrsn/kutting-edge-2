@include('orders/_partials/header')
	{{ Form::open('orders/finalization', 'post', array('class' => 'item')) }}
	<?php $carted = CartProducts::where('shopper', '=', $session_id)->get(); ?>
	@foreach( $carted as $cart )
		<?php $product = unserialize($cart->product); ?>
		<?php $item = Product::find($product['product_id']); ?>
		<?php 
			$thumb = explode('/', $item->file);
			$thumb = $thumb[0] . '/thumb_' . $thumb[1];
		?>
	<?php if ( isset( $product['options'] ) ) { ?>
	<div id="orderItems">
		<section id="product">
			<div class="left">
				<img src="{{ URL::base() }}/{{ $thumb }}" alt="{{ $item->title }}" width="156" height="156">
			</div>
			<div class="left" id="info">
				<div class="left">
					<h2>{{ $item->title }}</h2>
					@foreach( $product['options'] as $opt )
						<?php $opt = Options::find($opt); ?>
						<h3>{{ $opt->title }} - </h3> 
							<p>{{ $opt->description }}</p>
							<?php if ( $opt->form == '1' ) : ?>
								<textarea name="formOption[]" cols="50" rows="8"></textarea>
							<?php elseif ( $opt->form == '2' ) : ?>
								<input type="text" name="formOption[]">
							<?php endif; ?>
					@endforeach
			</div>
		</section>
	</div>
	<?php } ?>
	@endforeach
		<input type="hidden" name="session_id" value="{{ $session_id }}">
		<input type="hidden" name="client" value="{{ $client->id }}">
		<center style="padding:20px;margin-top:15px;">
			<button onclick="window.location='{{ URL::to('orders/cancel/') }}{{$session_id}}'" type="button">Cancel Order</button></a>
			<button type="submit">Finalization &raquo;</button>
		</center>
	{{ Form::close() }}
@include('orders/_partials/footer')