@include('orders/_partials/header')
<style type="text/css">
	th { font-weight: bold; }
</style>
<?php $products = CartProducts::where('shopper', '=', $cart->id)->get(); $sum = 0; ?>
<?php $info = unserialize($cart->info); ?>
<br><br>
				<table style="width:80%;margin:auto" border="1" cellspacing="2" cellpadding="3">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Color</th>
							<th>Size</th>
							<th>QTY</th>
							<th>Price</th>
						</tr>
					</thead>
					<?php $sum = 0; ?>
					@foreach( $products as $prod )
						<?php $prod = unserialize($prod->product); ?>
						<?php $product = Product::find($prod['product_id']); ?>
						<?php $color = Color::find($prod['color']); ?>
						<tr>
							<td>{{ $product->title }}</td>
							<td>{{ $color->title }}</td>
							<td><?php if ( $prod['sizes'] == 0 ) {} else { ?>{{ $prod['sizes'] }}<?php } ?></td>
							<td>{{ $prod['quantity'] }}</td>
							<?php $price = explode('$', $product->price) ?>
							<td>${{ money_format('%i', $prod['quantity'] * $price[1]) }}</td>
							<?php $sum += $prod['quantity'] * $price[1]; ?>
						</tr>
					@endforeach
				</table>
				<br>
				<table style="width:80%;margin:auto" border="1" cellspacing="2" cellpadding="3">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Custom Option/s</th>
							<th>Custom Value/s</th>
							<th>Price</th>
						</tr>
					</thead>
					@foreach ( $products as $prod )
						<?php $options = unserialize($prod->options); ?>
						<?php $prod = unserialize($prod->product); ?>
						<?php $product = Product::find($prod['product_id']); ?>
						<?php if ( isset($prod['options']) ) : ?>
						<tr>
							<td>{{ $product->title }}</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php $opts = Options::find($opt); ?>
									{{ $opts->title }}, 
								@endforeach
							</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php echo $options[$opt]; ?>, 
								@endforeach
							</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php $opts = Options::find($opt); ?>
									
									<?php $optprice = explode('$', $opts->price); ?>
									<?php $sum += $prod['quantity'] * $optprice[1]; ?>
									${{ money_format('%i', $optprice[1]) }},
								@endforeach
							</td>
						</tr>
					<?php endif; ?>
					@endforeach

				</table>
				<br>
				<table style="width:80%;margin:auto;" border="1">
					<thead>
						<tr>
							<th></th>
							<th>Price</th>
						</tr>
						<tr>
							<td>Sub Total</td>
							<td>{{ money_format('%i', $sum) }}</td>
						</tr>
						<tr>
							<td>Taxes (13%)</td>
							<?php $tax = $sum * .13 ?>
							<td>${{ money_format('%i',$tax) }}</td>
						</tr>
						<?php if ( $client->shipping > '' ) : ?>
						<tr>
							<td>Shipping Cost</td>
							<td>{{ $client->shipping }}</td>
							<?php $shipping = explode("$", $client->shipping) ?>
							<?php $shipping = $shipping[1]; ?>
						</tr>
						<?php else: $shipping = 0; endif; ?>
						<tr>
							<td>Total</td>
							<?php $total = $tax + $sum + $shipping; ?>
							<td>${{ money_format('%i', $total) }}</td>
						</tr>
					</thead>
				</table>
<form action="https://checkout.psigate.com/HTMLPost/HTMLMessenger" id="payment" method="POST">
			<input type="hidden" name="StoreKey" value="KuttingEdgedaIbD1Ga012117">
			<input type="hidden" name="CustomerRefNo" value="{{ $cart->id }}">
			<div style="width:80%;margin:auto;overflow:hidden">
				<div class="left">
					{{ Form::label('Bname', 'Billing Name') }}
					{{ Form::text('Bname', '', array('placeholder' => 'Your Name (Billing)', 'class' => 'required')) }}
					<br>
					{{ Form::label('Baddress1', 'Billing Address') }}
					{{ Form::text('Baddress1', '', array('class' => 'required')) }}
					<br>
					{{ Form::label('Baddress2', '') }}
					{{ Form::text('Baddress2') }}
					<br>
					{{ Form::label('Bcity', 'City') }}
					{{ Form::text('Bcity', '', array('class' => 'required')) }}
				</div>
				<div class="right">
					{{ Form::label('Bprovince', 'Province') }}
					{{ Form::text('Bprovince', '', array('class' => 'required')) }}
					<br>
					{{ Form::label('Bpostalcode', 'Postal Code') }}
					{{ Form::text('Bpostalcode', '', array('class' => 'required')) }}
					<br>
					{{ Form::label('Phone', 'Phone') }}
					{{ Form::text('Phone', '', array('class' => 'required')) }}
					<br>
					{{ Form::label('Email', 'Email') }}
					{{ Form::text('Email', '', array('class' => 'required')) }} <br>
				</div>
			</div>
			{{ Form::hidden('Bcompany', $client->customername)}}
			{{ Form::hidden('Bcountry', 'CA') }}
			{{ Form::hidden('SubTotal', money_format('%i', $total))}}

	<input type="hidden" name="testMode" value="1">
	<input type="hidden" name="ThanksURL" value="{{ URL::to('order/payment') }}">
	<input type="hidden" name="NoThanksURL" value="{{ URL::to('order/payment') }}">
	<input type="hidden" name="ResponseFormat" value="HTML">
	<br>
	<center>
		<button type="submit">Pay Now</button>
	</center>
</form>
@include('orders/_partials/footer')