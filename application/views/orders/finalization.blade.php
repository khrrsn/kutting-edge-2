<?php $org = Organization::find($client->organization); ?>
@include('orders/_partials/header')
<div id="orderItems">
			<?php $carted = CartProducts::where('shopper', '=', $session_id)->get(); $sum = 0; ?>
			@foreach ( $carted as $cart )
				<?php $product = unserialize($cart->product); ?>
				<?php $item = Product::find($product['product_id']); ?>
				<?php $price = explode('$', $item->price) ?>
				<?php $priced = $price[1] * $product['quantity']; ?>
				<?php $sum += $priced; ?>
				<?php 
					$thumb = explode('/', $item->file);
					$thumb = $thumb[0] . '/thumb_' . $thumb[1];
				?>

				<section id="product" class="product_{{ $cart->id }}">
					<div class="left">
						<img src="{{ URL::base() }}/{{ $thumb }}" alt="">
					</div>
					<div class="left" id="info">
						<div class="left">
							<h2>{{ $item->title }}</h2>
							<?php $color = Color::find($product['color']);  ?>
							Color: {{ $color->title }} <?php if ( $product['sizes'] == 0 ) {} else { ?>| Size: {{ $product['sizes'] }}<?php } ?> | Price: {{$item->price }}
							<br>
							<?php if (isset($product['options'])) { ?>
							Custom Options
								<ul>
							@foreach( $product['options'] as $opt )
								<?php $option = Options::find($opt);
									$optPrice = explode('$', $option->price);
									$sum += $optPrice[1];
								?>
									<li>{{ $option->title }}</li>
							@endforeach
								</ul>
							<?php } ?>
							<span class="remove" onclick="removeFromCart('{{ $cart->id }}');">Remove Item</span>
					</div>
				</section>
			@endforeach
	</div>
<?php if ( $client->payment == 1 ) : ?>
{{ Form::open('orders/confirm', 'post', array('class' => 'finalization')) }}
<?php elseif ( $client->payment == 2 ) : ?>
{{ Form::open('orders/payment', 'post', array('class' => 'finalization')) }}
<?php endif; ?>
	<input type="hidden" name="session_id" value="{{ Input::get('session_id') }}">
	<input type="hidden" name="client" value="{{ Input::get('client') }}">
	<div style="padding:15px">
		<div class="total">
			<table width="160">
				<tr>
					<th><b>Subtotal</b></th>
					<th>${{ $sum }}</th>
				</tr>
				<tr>
					<td><b>Taxes (13%)</b></td>
					<td>$<?php $tax = $sum * .13; $tax = round($tax, 2); $tax = money_format('%i', $tax); ?>{{ $tax }}</td>
				</tr>
				<?php if ( $client->shipping > '' ) : ?>
				<tr>
					<td><b>Shipping Cost</b></td>
					<td>{{ $client->shipping }}</td>
					<?php $shipping = explode("$", $client->shipping) ?>
					<?php $shipping = $shipping[1]; ?>
				</tr>
				<?php else: $shipping = 0; endif; ?>
				<tr>
					<td><b>Total</b></td>
					<?php $total = $tax + $sum + $shipping; ?>
					<td>${{ money_format('%i', $total) }}</td>
				</tr>
			
			</table>
		</div>
		<?php $orderer = CartSession::find($session_id); ?>
		<?php $orderer = unserialize($orderer->info); ?>
		<?php if ( $client->shipping > '' ) : ?>
		<br>
			<input type="radio" name="shipping" onclick="noshipping('1');"  value="1"> Pick Up.  Kutting Edge will send you a pick up notice to your provided email when your order is available. All orders can be picked up at the address stated below) <br>
			<input type="radio" name="shipping" checked onclick="noshipping('2');"  value="2"> Delivery.
		<br>
		<?php endif; ?>
		<table>
			<tr>
				<td><input type="checkbox" name="everything" required></td>
				<td>I agree to the terms and conditions of submitting this form.</td>
			</tr>
			<tr>
				<td></td>
				<td>I understand that in order to cancel this order I must contact Kutting Edge within 24 hours of the submission of the original purchase.</td>
			</tr>
		<table>
	</div>
	<br>
	<center>
		<button style="font-size:16px;padding:10px;height:auto">
			<?php if ( $client->payment == 1 ) : ?>
			Confirm Order
			<?php elseif ( $client->payment == 2 ) : ?>
			Payment Processing
			<?php endif; ?>
		</button>
		<br><br>
		<button onclick="window.location='{{ URL::to('orders/cancel/') }}{{$session_id}}'" type="button">Cancel Order</button></a>
	</center>
{{ Form::close() }}
<script type="text/javascript">
		function removeFromCart(product_id) {
			var num = $('input[name="shipping"]:checked').val();
			var dataString = {id: product_id};
			var request = $.ajax({
				type: "post",
				url: "{{ URL::to('orders/remove/') }}",
				data: dataString,
			});
			request.done(function(msg) {
				console.log(num);
				$('section#product.product_' + product_id).fadeOut();
				$('.total').load('{{ URL::to('orders/updateTotal/') }}{{$session_id}}/' + num);
			});
			return false;
		}
		function noshipping( num ) {
			if ( num == 1 ) {
				$('.total').load('{{ URL::to('orders/updateTotal/') }}{{$session_id}}/1');
			}
			if ( num == 2 ) {
				$('.total').load('{{ URL::to('orders/updateTotal/') }}{{$session_id}}/2');
			}
		}
</script>
@include('orders/_partials/footer')