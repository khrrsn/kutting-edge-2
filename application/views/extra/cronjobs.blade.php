<table>
	
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Order Start</th>
		<th>Order Finish</th>
	</tr>

	@foreach ( $clients as $client )
		<tr>
			<td>{{ $client->customername }}</td>
			<td>{{ $client->email }}</td>
			<td>{{ $client->order_start }}</td>
			<td>{{ $client->order_finish }}</td>
			<?php
				$sevendays = new DateTime($client->order_finish);
				$sevendays->sub(new DateInterval('P7D'));
				$sevendays = $sevendays->format('Y-m-d');

				$fromUser = new DateTime(date("Y-m-d"));
				if ( $fromUser == new DateTime($client->order_start) ) {
					$mailer = IoC::resolve('phpmailer');
					$message = 'This is a test, thank you for your time!';
					try {
					    $mailer->AddAddress( $client->email, $client->customername );
					    $mailer->Subject  = "Kutting Edge: Your Order Has Started!";
					    $mailer->IsHTML(true);
					    $mailer->Body = View::make('extra/email')->with('client', $client)->with('message', $message);
					    $mailer->Send();
					    $mailer->ClearAllRecipients();
					} catch (Exception $e) {
					    echo 'Message was not sent.';
					    echo 'Mailer error: ' . $e->getMessage();
					}
				}
				if ( $fromUser == new DateTime($client->order_finish) ) {
					$mailer = IoC::resolve('phpmailer');
					$message = 'Your order has finished!';
					try {
						$mailer->AddAddress( $client->email, $client->customername );
						$mailer->Subject = "Kutting Edge: Your Order Has Finished!";
						$mailer->IsHTML(true);
						$mailer->Body = View::make('extra/email')->with('client', $client)->with('message', $message);
						$mailer->Send();
						$mailer->ClearAllRecipients();
					} catch ( Exception $e ) {
						echo 'Message was not sent.';
						echo 'Mailer error: ' . $e->getMessage();
					}
				}
				if ( $fromUser == new DateTime($sevendays) ) {
					$mailer = IoC::resolve('phpmailer');
					$message = 'Your order has 7 days Left!';
					try {
						$mailer->AddAddress( $client->email, $client->customername );
						$mailer->Subject = "Kutting Edge: Your Order Has 7 Days Left!";
						$mailer->IsHTML(true);
						$mailer->Body = View::make('extra/email')->with('client', $client)->with('message', $message);
						$mailer->Send();
						$mailer->ClearAllRecipients();
					} catch ( Exception $e ) {
						echo 'Message was not sent.';
						echo 'Mailer error: ' . $e->getMessage();
					}
				}
			?>
		</tr>
	@endforeach

</table>