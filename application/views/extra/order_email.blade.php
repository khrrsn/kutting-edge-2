<?php $products = CartProducts::where('shopper', '=', $cart->id)->get(); $sum = 0; ?>
<?php $info = unserialize($cart->info); ?>
	<table width="100%">
		<tr>
			<td>
				<img src="http://clients.dev/kutting_edge/public/images/header_logo.png" alt="">
			</td>
		</tr>
		<tr>
			<td style="font-family:Helvetica,Arial,sans-serif;font-size: 14px;line-height:200%">
				Congratulations, {{ $info['firstName'] }} {{ $info['lastName'] }} your order was successful.

				<?php $client = Client::find($cart->client); ?>

				<p>
					{{ $client->customername }} <br>
					{{ $client->billingaddress1 }} <br>
					{{ $client->billingaddress2 }} <br>
					{{ $client->billingaddress3 }}
				</p>

				<p>&nbsp;</p>
				<table style="width:100%" border="1" cellspacing="2" cellpadding="3">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Color</th>
							<th>Size</th>
							<th>QTY</th>
							<th>Price</th>
						</tr>
					</thead>
					<?php $sum = 0; ?>
					@foreach( $products as $prod )
						<?php $prod = unserialize($prod->product); ?>
						<?php $product = Product::find($prod['product_id']); ?>
						<?php $color = Color::find($prod['color']); ?>
						<tr>
							<td>{{ $product->title }}</td>
							<td>{{ $color->title }}</td>
							<td><?php if ( $prod['sizes'] == 0 ) {} else { ?>{{ $prod['sizes'] }}<?php } ?></td>
							<td>{{ $prod['quantity'] }}</td>
							<?php $price = explode('$', $product->price) ?>
							<td>${{ money_format('%i', $prod['quantity'] * $price[1]) }}</td>
							<?php $sum += $prod['quantity'] * $price[1]; ?>
						</tr>
					@endforeach
				</table>
				<br>
				<table style="width:100%" border="1" cellspacing="2" cellpadding="3">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Custom Option/s</th>
							<th>Custom Value/s</th>
							<th>Price</th>
						</tr>
					</thead>
					@foreach ( $products as $prod )
						<?php $options = unserialize($prod->options); ?>
						<?php $prod = unserialize($prod->product); ?>
						<?php $product = Product::find($prod['product_id']); ?>
						<?php if ( isset($prod['options']) ) : ?>
						<tr>
							<td>{{ $product->title }}</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php $opts = Options::find($opt); ?>
									{{ $opts->title }}, 
								@endforeach
							</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php echo $options[$opt]; ?>, 
								@endforeach
							</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php $opts = Options::find($opt); ?>
									
									<?php $optprice = explode('$', $opts->price); ?>
									<?php $sum += $prod['quantity'] * $optprice[1]; ?>
									${{ money_format('%i', $optprice[1]) }},
								@endforeach
							</td>
						</tr>
					<?php endif; ?>
					@endforeach

				</table>
				<br>
				<table style="width:100%;" border="1">
					<thead>
						<tr>
							<th></th>
							<th>Price</th>
						</tr>
						<tr>
							<td>Sub Total</td>
							<td>{{ money_format('%i', $sum) }}</td>
						</tr>
						<tr>
							<td>Taxes (13%)</td>
							<?php $tax = $sum * .13 ?>
							<td>${{ money_format('%i',$tax) }}</td>
						</tr>
						<?php if ( $client->shipping > '' ) : ?>
						<tr>
							<td><b>Shipping Cost</b></td>
							<td>{{ $client->shipping }}</td>
							<?php $shipping = explode("$", $client->shipping) ?>
							<?php $shipping = $shipping[1]; ?>
						</tr>
						<?php else: $shipping = 0; endif; ?>
						<tr>
							<td>Total</td>
							<?php $total = $tax + $sum + $shipping; ?>
							<td>${{ money_format('%i', $total) }}</td>
						</tr>
					</thead>
				</table>
				<br>
				<p>&nbsp;</p>
		</td>
		</tr>
		<tr>
			<td style="font-family:Helvetica,Arial,sans-serif;font-size: 14px;line-height:100%">

				<p>If you have any trouble, please contact us at (333) 222-1111 or e-mail us at <a href="kuttdean&#64;bellnet.ca">kuttdean&#64;bellnet.ca</a> and use this number {{ $cart->id }}</p>

				<p>Thank you for your time, Please print a copy of this, for your records.</p>
				Kutting Edge Inc. <br>
				3 Progress Drive, <br>
				Unit 4 Orillia, <br>
				ON L3V 6H1
			</td>
		</tr>
	</table>