@include('dashboard/_partials/header')
	<div class="full">
		<h2>
			{{ $client->customername }}
			<div class="right">
				<button onclick="window.location='{{ URL::to('dashboard/clients/delete/') }}{{ $client->id }}'">Remove Client</button>
				<button>Send Email</button>
			</div>
		</h2>
				<form method="post" action="{{ URL::to('dashboard/clients/update') }}" class="customer" enctype="multipart/form-data" accept-charset="UTF-8"  onsubmit="return selectAll( new Array( 'location_right' ) );">
			<input type="hidden" name="id" value="{{ $client->id }}">
			<div class="padding">
				<label for="customerName">Customer Name</label>
				<input type="text" name="customerName" value="{{ $client->customername }}">
			</div>
			<table>
				<tr>
					<td>
						<label for="billingAddress">Billing Address</label>
						<input type="text" name="billingAddress1" value="{{ $client->billingaddress1 }}"> <br>
						<input type="text" name="billingAddress2" value="{{ $client->billingaddress2 }}"> <br>
						<input type="text" name="billingAddress3" value="{{ $client->billingaddress3 }}">
					</td>
					<td style="vertical-align:top">
						<label for="city">City</label>
						<input type="text" name="city" value="{{ $client->city }}">
						<label for="province">Province</label>
						<input type="text" name="province"value="{{ $client->province }}">
					</td>
					<td style="vertical-align:top">
						<label for="postal">Postal Code</label>
						<input type="text" name="postal" value="{{ $client->postal }}">
					</td>
				</tr>
			</table>
			<table>
				<tr>
					
					<td>
						<label for="email">Email</label>
						<input type="email" size="35" name="email" value="{{ $client->email }}">
					</td>
					<td>
						<label for="emailConfirm">Confirm</label>
						<input type="email"  size="35" name="emailConfirm" value="{{ $client->email }}">
					</td>

				</tr>
			</table>

			<div class="padding">
				<label for="organization">Organization</label>
				<select name="organization">
					<?php $organization = Organization::all(); ?>
					@foreach ( $organization as $org )
					<option value="{{ $org->id }}"<?php 
						if ( $org->id == $client->organization) { echo ' selected'; } ?>>{{ $org->organization }}</option>
					@endforeach
				</select>
			</div>

			<table>
				<tr>
					<td>
						<label for="orderStart">Order Start</label>
						<input type="date" name="order_start" value="{{ $client->order_start }}">
					</td>
					<td>
						<label for="orderFinish">Order Finish</label>
						<input type="date" name="order_finish" value="{{ $client->order_finish }}">
					</td>
				</tr>
			</table>

			<div class="padding">
				<label for="coloursAvailable">Colours Available</label>
				
				<div class="colours">
					<?php $colors = Color::all();
						$curColors = unserialize($client->colours);
					?>
					@foreach ( $colors as $color )
						<div>
							<span class="color" style="background:{{$color->hex}}"></span>
							<input type="checkbox" name="colours[]" value="{{$color->id}}"<?php
								foreach ($curColors as $curcol) {
									if ($color->id == $curcol) {
										echo ' checked';
									}
								}
							 ?>>
						</div>
					@endforeach				
				</div>
			</div>

			<div class="padding">
				<label for="shipping">Shipping Cost:</label>
				<input type="checkbox" name="ifshipping"<?php if ( $client->shipping > '' ) echo ' checked'; ?>> Please check this if you are allowing shipping, if not, please ignore.
				<br>
				<input type="text" name="shippingcost" value="{{ $client->shipping }}">
			</div>
			<div class="padding">
				<label for="payment">Payment Method</label>
				<select name="payment">
					<option value="1"<?php if ($client->payment == '1') echo ' selected'; ?>>Client Payment</option>
					<option value="2"<?php if ($client->payment == '2') echo ' selected'; ?>>Customer Payment</option>
				</select>
			</div>
			<div class="padding">
				<label for="custom">Choose Items Available</label>
			</div>
				<table>
					<tr>
						<td>
							<select size="15" name="location_left" id="location_left" multiple="multiple">
								<?php $product = Product::all(); ?>
								@foreach ($product as $prod)
									<option value="{{ $prod->id }}">{{ $prod->title }}</option>
								@endforeach
							</select>
						</td>
						<td>
							<button type="button" onclick="move( 'location_right', 'location_left')">&laquo;</button>
							<br><br>
							<button type="button" onclick="move( 'location_left', 'location_right')">&raquo;</button>
						</td>
						<td>
							<select size="15" name="location_right[]" id="location_right" multiple="multiple">
								<?php
								$productsSelected = unserialize($client->products);
								if ( is_array($productsSelected)) :
								$productarray = Product::where_in('id', unserialize($client->products))->get();
								?>
								@foreach( $productarray as $product )
									<option value="{{ $product->id }}">{{ $product->title }}</option>
								@endforeach
							<?php else: ?>
								<?php $product = Product::find($productsSelected); ?>
								<option value="{{ $product->id }}">{{ $product->title }}</option>
							</select>
						<?php endif; ?>
						</td>
					</tr>
				</table>
				<button type="submit">Update!</button>
		{{ Form::close() }}
		
	</div>
	{{ HTML::script('js/customOptions.js') }}
@include('dashboard/_partials/footer')