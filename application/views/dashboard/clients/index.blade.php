@include('dashboard/_partials/header')
	<?php 
		function strlim ( $string, $limit )
		{
			if ( strlen($string) > $limit )
			{
				$string = substr($string, 0, strrpos(substr($string, 0, $limit), ' ')) . '...';
				echo $string;
			} else
			{
				echo $string;
			}
		}
	?>
	<div class="clients full">
		<h2>
			Clients
			<div class="right">
				<button onclick="window.location='{{ URL::to('dashboard/clients/colors/') }}'">Manage Colors</button>
				<button onclick="window.location='{{ URL::to('dashboard/clients/organization/') }}'">Manage Organizations</button>
				<button onclick="window.location='{{ URL::to('dashboard/clients/add') }}'">Add Client</button>
			</div>
		</h2>
		<table>
			<thead>
				<tr>
					<th>Client Name</th>
					<th>Address</th>
					<th>Username</th>
					<th>Email Address</th>
					<th>Organization</th>
					<th>Order Start</th>
					<th>Order Finish</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
			<?php $clients = Client::all(); ?>
			@foreach ($clients as $client)
				<tr>
					<td>{{ strlim($client->customername, 15) }}</td>
					<td>{{ strlim($client->billingaddress1, 15) }}</td>
					<td>{{ $client->username }}</td>
					<td>{{ $client->email }}</td>
					<td><?php 
						$organization = Organization::find($client->organization);
						echo strlim($organization->organization, 30);
					?></td>
					<td>{{ $client->order_start }}</td>
					<td>{{ $client->order_finish }}</td>
					<td><button onclick="window.location='{{ URL::to('dashboard/clients/manage/') }}{{ $client->id }}'">Manage</button>
						<button onclick="window.location='{{ URL::to('orders/')}}{{$client->id}}'">Order Form</button>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')