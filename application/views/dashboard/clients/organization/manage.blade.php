@include('dashboard/_partials/header')
	<div class="clients full">
		<h2>{{ $org->organization }}
			<div class="right">
				<!-- <button onclick="window.location='{{ URL::to('dashboard/clients/organization/delete/') }}{{ $org->id }}'">Delete</button> -->
			</div>
		</h2>
		{{ Form::open('dashboard/clients/organization/update', 'POST', array('class' => 'customer')) }}
		<input type="hidden" value="{{ $org->id }}" name="id">
		<?php 
			$required = $org->required;
			$required = unserialize($required);
		?>
		<label for="organization">Organization Name:</label>
		<input type="text" name="organization" size="70" value="{{ $org->organization }}">

		<h3>Select Required Forms for Orders</h3>
			
			<div class="left">
				<label for="firstName">First Name</label>
				<input type="checkbox" name="required[]" value="firstName" <?php
					foreach ($required as $req) {
						if ($req == 'firstName') {
							echo 'checked';
						}
					}
				 ?>>

				<label for="address-1">Address</label>
				<input type="checkbox" name="required[]" value="address" <?php
					foreach ($required as $req) {
						if ($req == 'address') {
							echo 'checked';
						}
					}
				 ?>>

				<label for="homeroom">Homeroom Teacher</label>
				<input type="checkbox" name="required[]" value="teacher" <?php
					foreach ($required as $req) {
						if ($req == 'teacher') {
							echo 'checked';
						}
					}
				 ?>>
			</div>
			<div class="left" id="right">
				
				<div class="group">
					<span class="small">
						
						<label for="middle">Middle</label>
						<input type="checkbox" name="required[]" value="middleName" <?php
					foreach ($required as $req) {
						if ($req == 'middleName') {
							echo 'checked';
						}
					}
				 ?>>
					
					</span>
					
					<span class="small-after">
						
						<label for="lastName">Last Name</label>
						<input type="checkbox" name="required[]" value="lastName" <?php
					foreach ($required as $req) {
						if ($req == 'lastName') {
							echo 'checked';
						}
					}
				 ?>>
					
					</span>
				</div>
				<label for="phoneNumber">Phone Number</label>
				<input type="checkbox" name="required[]" value="phoneNumber" <?php
					foreach ($required as $req) {
						if ($req == 'phoneNumber') {
							echo 'checked';
						}
					}
				 ?>>

				<label for="email">Email Address</label>
				<input type="checkbox" name="required[]" value="email" <?php
					foreach ($required as $req) {
						if ($req == 'email') {
							echo 'checked';
						}
					}
				 ?>>
					
				<label for="room">Room</label>
				<input type="checkbox" name="required[]" value="room" <?php
					foreach ($required as $req) {
						if ($req == 'room') {
							echo 'checked';
						}
					}
				 ?>>


			</div>

			<div class="clear"></div>
			<?php $cf = unserialize($org->custom); ?>
			<h3>Manage Custom Fields:</h3>
			<label for="">Name:</label>
			<input type="text" name="custom[]" value="{{ $cf[0] }}">
			<label for="">Name:</label>
			<input type="text" name="custom[]" value="{{ $cf[1] }}">
			<label for="">Name:</label>
			<input type="text" name="custom[]" value="{{ $cf[2] }}">
			<label for="">Name:</label>
			<input type="text" name="custom[]" value="{{ $cf[3] }}">
			<label for="">Name:</label>
			<input type="text" name="custom[]" value="{{ $cf[4] }}">
			<p>
				<button type="submit">Update!</button>
			</p>

		{{ Form::close() }}
		</table>
	</div>
@include('dashboard/_partials/footer')