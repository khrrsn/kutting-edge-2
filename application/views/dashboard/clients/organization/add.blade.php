@include('dashboard/_partials/header')
	<div class="clients full">
		<h2>Create an Organization</h2>
		{{ Form::open('dashboard/clients/organization/create', 'POST', array('class' => 'customer')) }}

		<label for="organization">Organization Name:</label>
		<input type="text" name="organization" size="70">

		<h3>Select Required Forms for Orders</h3>
			
			<div class="left">
				<label for="firstName">First Name</label>
				<input type="checkbox" name="required[]" value="firstName" checked>

				<label for="address-1">Address</label>
				<input type="checkbox" name="required[]" value="address">

				<label for="homeroom">Homeroom Teacher</label>
				<input type="checkbox" name="required[]" value="teacher">
			</div>
			<div class="left" id="right">
				
				<div class="group">
					<span class="small">
						
						<label for="middle">Middle</label>
						<input type="checkbox" name="required[]" value="middleName">
					
					</span>
					
					<span class="small-after">
						
						<label for="lastName">Last Name</label>
						<input type="checkbox" name="required[]" value="lastName" checked>
					
					</span>
				</div>
				<label for="phoneNumber">Phone Number</label>
				<input type="checkbox" name="required[]" value="phoneNumber">

				<label for="email">Email Address</label>
				<input type="checkbox" name="required[]" value="email" checked>
					
				<label for="room">Room</label>
				<input type="checkbox" name="required[]" value="room">


			</div><div class="clear"></div>

			<h3>Add a Field:</h3>
			<label for="">Name:</label>
			<input type="text" name="custom[]">
			<label for="">Name:</label>
			<input type="text" name="custom[]">
			<label for="">Name:</label>
			<input type="text" name="custom[]">
			<label for="">Name:</label>
			<input type="text" name="custom[]">
			<label for="">Name:</label>
			<input type="text" name="custom[]">

			<div class="clear"></div>
			<p>
				<button type="submit">Submit</button>
			</p>

		{{ Form::close() }}
		</table>
	</div>
@include('dashboard/_partials/footer')