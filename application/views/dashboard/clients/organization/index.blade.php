@include('dashboard/_partials/header')
	<div class="clients full">
		<h2>
			Organizations
			<div class="right">
				<button onclick="window.location='{{ URL::to('dashboard/clients/') }}'">&laquo; Back to Clients</button>
				<button onclick="window.location='{{ URL::to('dashboard/clients/organization/add') }}'">Add Organization</button>
			</div>
		</h2>
		<table>
			<thead>
				<tr>
					<th>Organization Name</th>
					<th>Required Forms</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
			<?php $orgs = Organization::all(); ?>
			@foreach ($orgs as $org)
				<tr>
					<td>{{ $org->organization }}</td>
					<td>
						<?php $required = unserialize($org->required); ?>
						@foreach ($required as $req)
							{{ $req }}, 
						@endforeach

					</td>
					<td><button onclick="window.location='{{ URL::to('dashboard/clients/organization/manage/') }}{{ $org->id }}'">Manage</button></td>
				</tr>
			@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')