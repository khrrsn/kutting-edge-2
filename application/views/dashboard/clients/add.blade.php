@include('dashboard/_partials/header')
<script type="text/javascript">
	$('button').click(function() {
		$('form').submit();
	});
</script>

	<div class="full">
		
		<h2>Add New Customer

			<div class="right">
				<a href="../"><button>&laquo; Back</button></a>
				<button class="save">Save</button>
			</div>
		</h2>

		<form method="post" action="{{ URL::to('dashboard/clients/add') }}" class="customer"  onsubmit="return selectAll( new Array( 'location_right' ) );">

			<?php if ($errors->has()) : ?>
			<div style="width:300px;margin-bottom:20px">
				@foreach ( $errors->all() as $error )
					<span class="error">{{$error}}</span>
				@endforeach
			</div>
			<?php endif; ?>
			
			<div class="padding">
				<label for="customerName">Customer Name</label>
				<input type="text" name="customerName" required>
			</div>
			<table>
				<tr>
					<td>
						<label for="billingAddress">Billing Address</label>
						<input type="text" name="billingAddress1" required> <br>
						<input type="text" name="billingAddress2"> <br>
						<input type="text" name="billingAddress3">
					</td>
					<td style="vertical-align:top">
						<label for="city">City</label>
						<input type="text" name="city" required>
						<label for="province">Province</label>
						<input type="text" name="province" required>
					</td>
					<td style="vertical-align:top">
						<label for="postal">Postal Code</label>
						<input type="text" name="postal" required>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>
						<label for="username">Username</label>
						<input type="text" name="username" required>
					</td>
					<td>
						<label for="password">Password</label>
						<input type="password" name="password" required>
					</td>
					<td>	
						<label for="password_confirmation">Confirm</label>
						<input type="password" name="password_confirmation" required>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					
					<td>
						<label for="email">Email</label>
						<input type="email" size="35" name="email" required>
					</td>
					<td>
						<label for="emailConfirm">Confirm</label>
						<input type="email"  size="35" name="email_confirmation" required>
					</td>

				</tr>
			</table>

			<div class="padding">
				<label for="organization">Organization</label>
				<select name="organization">
					<?php $organization = Organization::all(); ?>
					@foreach ( $organization as $org )
					<option value="{{ $org->id }}">{{ $org->organization }}</option>
					@endforeach
				</select>
			</div>

			<table>
				<tr>
					<td>
						<label for="orderStart">Order Start</label>
						<input type="date" id="date" name="order_start" required>
					</td>
					<td>
						<label for="orderFinish">Order Finish</label>
						<input type="date" id="date2" name="order_finish" required>
					</td>
				</tr>
			</table>

			<div class="padding">
				<label for="coloursAvailable">Colours Available</label>
				
				<div class="colours">
					<?php $colors = Color::all(); ?>
					@foreach ( $colors as $color )
						<div>
							<span class="color" style="background:{{$color->hex}}"></span>
							<input type="checkbox" name="colours[]" value="{{$color->id}}">
						</div>
					@endforeach	
				</div>
			</div>
			<div class="padding">
				<label for="shipping">Shipping Cost:</label>
				<input type="checkbox" name="ifshipping"> Please check this if you are allowing shipping, if not, please ignore.
				<br>
				<input type="text" name="shippingcost" value="">
			</div>

			<div class="padding">
				<label for="payment">Payment Method</label>
				<select name="payment">
					<option value="1">Client Payment</option>
					<option value="2">Customer Payment</option>
				</select>
			</div>

			<div class="padding">
				<label for="custom">Choose Items Available</label>
			</div>
				<table>
					<tr>
						<td>
							<select size="15" name="location_left" id="location_left" multiple="multiple">
								<?php $product = Product::all(); ?>
								@foreach ($product as $prod)
									<option value="{{ $prod->id }}">{{ $prod->title }}</option>
								@endforeach
							</select>
						</td>
						<td>
							<button type="button" onclick="move( 'location_right', 'location_left')">&laquo;</button>
							<br><br>
							<button type="button" onclick="move( 'location_left', 'location_right')">&raquo;</button>
						</td>
						<td>
							<select size="15" name="location_right" id="location_right" multiple="multiple" required>
							</select>
						</td>
					</tr>
				</table>
				<input type="submit">
		</form>

	</div>
{{ HTML::script('js/customOptions.js') }}
<script type="text/javascript">
	$('button.save').click(function() {
		$('form.customer').submit();
	});
</script>
@include('dashboard/_partials/footer')