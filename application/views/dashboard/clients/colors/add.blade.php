@include('dashboard/_partials/header')
	{{ HTML::style('css/jquery.miniColors.css') }}
	<div class="clients full">
		<h2>Create a Color</h2>
		{{ Form::open('dashboard/clients/colors/add', 'POST', array('class' => 'customer')) }}

		<label for="title">Color Name:</label>
		<input type="text" name="title" size="40">

		<p>
			<input type="text" name="color" id="color" size="7">
		</p>

			<div class="clear"></div>
			<p>
				<button type="submit">Submit</button>
			</p>

		{{ Form::close() }}
		</table>
	</div>
	{{ HTML::script('js/jquery.miniColors.min.js') }}
	<script type="text/javascript">
		$('#color').miniColors();
	</script>
@include('dashboard/_partials/footer')