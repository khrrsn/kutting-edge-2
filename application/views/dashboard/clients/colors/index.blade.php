@include('dashboard/_partials/header')
	<div class="clients full">
		<h2>
			Organizations
			<div class="right">
				<button onclick="window.location='{{ URL::to('dashboard/clients/') }}'">&laquo; Back to Clients</button>
				<button onclick="window.location='{{ URL::to('dashboard/clients/colors/add') }}'">Add Color</button>
			</div>
		</h2>
		<?php $colors = Color::all(); ?>
		@foreach ( $colors as $color )
			<div style="width:100px;height:100px;float:left;background:{{$color->hex}}"></div>
		@endforeach
	</div>
@include('dashboard/_partials/footer')