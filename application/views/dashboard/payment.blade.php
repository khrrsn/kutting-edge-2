@include('dashboard/_partials/header')
	<div class="clients">
		<h2>
			Payment: {{ Input::get('Approved') }}
		</h2>
		<?php if (Input::get('Approved') == 'DECLINED') : ?>
			<h3>I'm sorry, Your payment has been declined. Here is some more information:</h3>
			<table>
				<thead>
					<tr>
						<th>Title</th>
						<th>Value</th>
					</tr>
				</thead>
				<tr>
					<td>Transaction Time</td>
					<td>{{ Input::get('TransTime') }}</td>
				</tr>
				<tr>
					<td>Order ID</td>
					<td>{{ Input::get('OrderID') }}</td>
				</tr>
				<tr>
					<td>Error Message</td>
					<td>{{ Input::get('ErrMsg') }}</td>
				</tr>
				<tr>
					<td>Payment Type</td>
					<td>{{ Input::get('PaymentType') }}</td>
				</tr>
				<tr>
					<td>Card Number</td>
					<td>{{ Input::get('CardNumber') }}</td>
				</tr>
				<tr>
					<td>Price</td>
					<td>${{ Input::get('FullTotal') }}</td>
				</tr>

			</table>
			<br>
			<p>Please <strong><a style="color:#000" href="{{ URL::to('dashboard') }}">Go Back</a></strong> and try again.</p>
		<?php elseif ( Input::get('Approved' == 'APPROVED') ) : ?>

			<?php
				$payment = Payment::create(
					array(
						'payment' => Input::get('FullTotal'),
						'client' => Input::get('CustomerRefNo'),
						'info' => serialize(Input::all()),
					)
				);
				if ( $payment ) {
					return Redirect::to('/');
				}
			?>

		<?php endif; ?>

	</div>
@include('dashboard/_partials/footer')