<?php include('_partials/header.php') ?>

	<div class="full">
		
		<h2>Add New Customer

			<div class="right">
				<button>&laquo; Back</button>
				<button>Save</button>
			</div>
		</h2>

		<form class="customer" action="">
			
			<div class="padding">
				<label for="customerName">Customer Name</label>
				<input type="text" name="customerName">
			</div>
			<table>
				<tr>
					<td>
						<label for="billingAddress">Billing Address</label>
						<input type="text" name="billingAddress1"> <br>
						<input type="text" name="billingAddress2"> <br>
						<input type="text" name="billingAddress3">
					</td>
					<td style="vertical-align:top">
						<label for="city">City</label>
						<input type="text" name="city">
						<label for="providence">Province</label>
						<input type="text" name="province">
					</td>
					<td style="vertical-align:top">
						<label for="postal">Postal Code</label>
						<input type="text" name="postal">
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>
						<label for="username">Username</label>
						<input type="text" name="username">
					</td>
					<td>
						<label for="password">Password</label>
						<input type="password" name="password">
					</td>
					<td>	
						<label for="confirm">Confirm</label>
						<input type="password" name="confirm">
					</td>
				</tr>
			</table>
			<table>
				<tr>
					
					<td>
						<label for="email">Email</label>
						<input type="email" size="35" name="email">
					</td>
					<td>
						<label for="emailConfirm">Confirm</label>
						<input type="email"  size="35" name="emailConfirm">
					</td>

				</tr>
			</table>

			<div class="padding">
				<label for="organization">Organization</label>
				<select name="organization">
					<option value="personal">Personal</option>
					<option value="business">Business</option>
					<option value="school">School</option>
				</select>
			</div>

			<table>
				<tr>
					<td>
						<label for="orderStart">Order Start</label>
						<input type="date" name="orderStart">
					</td>
					<td>
						<label for="orderFinish">Order Finish</label>
						<input type="date" name="orderFinish">
					</td>
				</tr>
			</table>

			<div class="padding">
				<label for="coloursAvailable">Colours Available</label>
				
				<div class="colours">
					<div>
						<span class="color" style="background:red"></span>
						<input type="checkbox">
					</div>
					<div>
						<span class="color" style="background:orange"></span>
						<input type="checkbox">
					</div>
					<div>
						<span class="color" style="background:yellow"></span>
						<input type="checkbox">
					</div>
					<div>
						<span class="color" style="background:green"></span>
						<input type="checkbox">
					</div>
					<div>
						<span class="color" style="background:blue"></span>
						<input type="checkbox">
					</div>
					<div>
						<span class="color" style="background:indigo"></span>
						<input type="checkbox">
					</div>
					<div>
						<span class="color" style="background:violet"></span>
						<input type="checkbox">
					</div>
				
				</div>
			</div>

			<div class="padding">
				<label for="custom">Choose Items Available</label>
			</div>
				<table>
					<tr>
						<td>
							<select size="15" name="select-choice" id="select-choice">
								<option value="Choice 1">Choice 1</option>
								<option value="Choice 2">Choice 2</option>
								<option value="Choice 3">Choice 3</option>
							</select>
						</td>
						<td>
							<button>&laquo;</button>
							<br><br>
							<button>&raquo;</button>
						</td>
						<td>
							<select size="15" name="select-choice" id="select-choice">
								<option value="Choice 1">Choice 1</option>
								<option value="Choice 2">Choice 2</option>
								<option value="Choice 3">Choice 3</option>
							</select>
						</td>
					</tr>
				</table>

		</form>

	</div>

<?php include('_partials/footer.php') ?>