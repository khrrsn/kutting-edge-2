<?php include('../_partials/header.php'); ?>
	<div class="clients">
		<h2>Client Orders</h2>
		<table>
			<tr>
				<th>Client Name</th>
				<th>Address</th>
				<th>Phone Number</th>
				<th>Email Address</th>
				<th>Items Ordered</th>
				<th>Order Total</th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<td>Test Name</td>
				<td>Test Address</td>
				<td>555-555-5555</td>
				<td>test@example.com</td>
				<td>50</td>
				<td>30</td>
				<td><button>Send Email</button></td>
			</tr>
		</table>
	</div>
<?php include('../_partials/footer.php'); ?>