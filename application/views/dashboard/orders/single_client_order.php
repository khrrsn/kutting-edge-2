<?php include('../_partials/header.php'); ?>
	<div class="clients">
		<h2>Frank Sinatra's Order</h2>

		<p>3220 Address Line <br>
			Orillia, Ontario <br>
			L3V 636</p>
		
		<p>Organization (Personal, Business, etc) <br>
			Order Finish Date</p>
		
		<h3>Sort by Items</h3>
		<table>
			<tr>
				<th>Completed?</th>
				<th>Item #</th>
				<th>Item Name</th>
				<th>Qty.</th>
				<th># w Custom Design</th>
				<th>Subtotal</th>
				<th>Taxes</th>
				<th>Total</th>
			</tr>
			<tr>
				<td>No</td>
				<td>54545</td>
				<td>Gildan T-Shirt</td>
				<td>20</td>
				<td>5</td>
				<td>3000.00</td>
				<td>300</td>
				<td>3300.00</td>
				<td><button class="small">View All</button></td>
			</tr>
		</table>
	</div>
<?php include('../_partials/footer.php'); ?>