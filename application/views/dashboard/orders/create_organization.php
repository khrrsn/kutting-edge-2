<?php include('../_partials/header.php'); ?>
	<div class="clients" id="form-view">
		<h2>Create an Organization</h2>
		<form action="">
			
			<div class="left">
				<label for="firstName">First Name</label>
				<input type="text" placeholder="Text Holder" name="firstName">

				<label for="address-1">Address</label>
				<input type="text" placeholder="Text Holder" name="address-1">
				<input type="text" placeholder="Text Holder" name="address-2">
				<input type="text" placeholder="Text Holder" name="address-3">

				<label for="homeroom">Homeroom Teacher</label>
				<input type="text" name="homeroom" placeholder="Text Holder">
			</div>
			<div class="left" id="right">
				
				<div class="group">
					<span class="small">
						
						<label for="middle">Middle</label>
						<input type="text" name="middle" placeholder="A.">
					
					</span>
					
					<span class="small-after">
						
						<label for="lastName">Last Name</label>
						<input type="text" name="lastName" placeholder="Text Holder">
					
					</span>
				</div>
				<label for="phoneNumber">Phone Number</label>
				<input type="text" name="phoneNumber" placeholder="Text Holder">

				<label for="email">Email Address</label>
				<input type="text" name="email" placeholder="Text Holder">

				<span class="small">
					
					<label for="room">Room</label>
					<input type="text" name="room" placeholder="A.">

				</span>

			</div>

		</form>
		</table>
	</div>
<?php include('../_partials/footer.php'); ?>