@include('dashboard/_partials/header')
	<div class="clients">
		<h2>
			Search Results for: {{ $search }}
		</h2>
		<table>
			<thead>
				<tr>
					<th>Client Name</th>
					<th>Address</th>
					<th>Organization</th>
					<th>Order Start</th>
					<th>Order Finish</th>
					<th>Total Sales</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
				<tr>
				@foreach ( $clients as $client)
				<td>{{ $client->customername }}</td>
					<td>{{ $client->email }}</td>
					<td><?php 
						$organization = Organization::find($client->organization);
						echo $organization->organization;
					?></td>
					<td>{{ $client->order_start }}</td>
					<td>{{ $client->order_finish }}</td>
					<td>
						<?php $totalSales = CartSession::where('client', '=', $client->id)->count(); ?>
						{{ $totalSales }}
					</td>
				<td><a href="{{ URL::to('dashboard/tracking/client/') }}{{ $client->id }}"><button>View</button></a></td>
				</tr>
				@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')