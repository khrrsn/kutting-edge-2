<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Kutting Edge: Dashboard</title>
	{{ HTML::style('css/global.css'); }}
	{{ HTML::style('css/dashboard.css'); }}
	<link rel="shortcut icon" href="<?php echo URL::to('favicon.ico'); ?>">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.1/modernizr.min.js"></script>
	{{ HTML::script('js/jquery-ui-1.8.22.custom.min.js') }}
	{{ HTML::style('css/jquery-ui-1.8.22.custom.css') }}
	{{ HTML::script('js/jquery.validate.min.js') }}
	{{ HTML::script('js/jquery.dataTables.min.js')}}
	{{ HTML::style('css/jquery.dataTables.css')}}
	<?php if ( Auth::user()->role == '2' ) : ?>
		{{ HTML::script('assets/easyCheckout3.js') }}
		{{ HTML::script('assets/tinybox2/tinybox.js') }}
	<?php endif; ?>
	<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<header>
		<div class="container">
			<a href="{{ URL::to('dashboard') }}"><h1>Kutting Edge</h1></a>
			<div class="header_right">
				<p>Welcome, {{ Auth::user()->name }} </p>
				{{ HTML::link('logout', 'Logout') }}
			</div>
		</div>
		<nav>
			<div class="container">
				<ul>
					<?php if ( Auth::user()->role == '2' ) { ?>
					<li><a href="{{ URL::to('orders/') }}{{ Auth::user()->id }}">Order Form</a></li>
					<?php } else { ?>
					<li>{{ HTML::link('dashboard/products', 'Products') }}</li>
					<li>{{ HTML::link('dashboard/clients', 'Clients') }}</li>
					<li>{{ HTML::link('dashboard/tracking', 'Tracking') }}</li>
					<?php } ?>
				</ul>
				<div class="right">
					{{ Form::open('dashboard/search', 'get', array('id' => 'search'))}}
						<input type="search" name="s" class="search" placeholder="Keyword">
					{{ Form::close() }}
				</div>
			</div>
		</nav>
	</header>

	<div class="content container">