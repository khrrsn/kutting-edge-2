@include('dashboard/_partials/header')
	<div class="clients full">
		<h2>
			Products
			<div class="right">
				<button onclick="window.location='{{ URL::to('dashboard/products/options') }}'">Custom Options</button>
				<button onclick="window.location='{{ URL::to('dashboard/products/add') }}'">Add Product</button>
			</div>
		</h2>
		<table>
			<thead>
				<tr>
					<th>Product Name</th>
					<th>Price</th>
					<th>Sizes</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
			<?php $product = Product::all(); ?>
			@foreach($product as $prod)
			<tr>
				<td>{{ $prod->title }}</td>
				<td>{{ $prod->price }}</td>
				<td>
					<?php $sizes = unserialize($prod->sizes); ?>
						@foreach ($sizes as $size)
							{{ $size }}, 
						@endforeach
				</td>
				<td><button onclick="window.location='{{ URL::to('dashboard/products/manage/') }}{{ $prod->id }}'">Manage</button></td>
			</tr>
			@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')