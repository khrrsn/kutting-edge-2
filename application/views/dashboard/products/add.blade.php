@include('dashboard/_partials/header')

	<div class="full">
			<h2>
				Add New Product
				<div class="right">
					<button onclick="window.location='{{ URL::to('dashboard/products') }}'">&laquo; Back</button>
					<button class="save">Save</button>
				</div>
			</h2>
			
			<form method="post" action="{{ URL::to('dashboard/products/add') }}" class="addProduct" enctype="multipart/form-data" accept-charset="UTF-8"  onsubmit="return selectAll( new Array( 'location_right' ) );">


			<?php if ($errors->has()) : ?>
			<div style="width:300px;margin-bottom:20px">
				@foreach ( $errors->all() as $error )
					<span class="error">{{$error}}</span>
				@endforeach
			</div>
			<?php endif; ?>
				
				<label class="top" for="productTitle">Product Title</label>
				<input type="text" size="60" name="productTitle" value="{{ Input::old('productTitle') }}">

				<br>

				<label class="top" for="unitPrice">Unit Price</label>
				<input type="text" size="5" name="unitPrice"<?php if ( Input::old('unitPrice') == '' ) { ?> value="$" <?php } else { ?>value="{{ Input::old('unitPrice') }}"<?php } ?>>
				
				<br>

				<label for="sizes">Sizes Available</label>

				<div class="sizes">
					<?php if ( Input::old('size') != '' ) {
						$size = (Input::old('size'));
						print_r($size);
					} else {
						$size = array();
					} ?>
					<span>
						<label for="0">None</label>
						<input type="checkbox" name="size[]" value="0"<?php if ( in_array('0', $size) ) echo ' checked'; ?>>
					</span>
					<span>
						<label for="XS">YS</label>
						<input type="checkbox" name="size[]" value="YS"<?php if ( in_array('YS', $size) ) echo ' checked'; ?>>
					</span>
					<span>
						<label for="YM">YM</label>
						<input type="checkbox" name="size[]" value="YM"<?php if ( in_array('YM', $size) ) echo ' checked'; ?>>
					</span>
					<span>
						<label for="YL">YL</label>
						<input type="checkbox" name="size[]" value="YL"<?php if ( in_array('YL', $size) ) echo ' checked'; ?>>
					</span>
					<span>
						<label for="XS">XS</label>
						<input type="checkbox" name="size[]" value="XS"<?php if ( in_array('XS', $size) ) echo ' checked'; ?>>
					</span>

					<span>
						<label for="S">S</label>
						<input type="checkbox" name="size[]" value="S"<?php if ( in_array('S', $size) ) echo ' checked'; ?>>
					</span>

					<span>
						<label for="M">M</label>
						<input type="checkbox" name="size[]" value="M"<?php if ( in_array('M', $size) ) echo ' checked'; ?>>
					</span>
					<span>
						<label for="L">L</label>
						<input type="checkbox" name="size[]" value="L"<?php if ( in_array('L', $size) ) echo ' checked'; ?>>
					</span>

					<span>
						<label for="XL">XL</label>
						<input type="checkbox" name="size[]" value="XL"<?php if ( in_array('XL', $size) ) echo ' checked'; ?>>
					</span>

					<span>
						<label for="XXL">XXL</label>
						<input type="checkbox" name="size[]" value="XXL"<?php if ( in_array('XXL', $size) ) echo ' checked'; ?>>
					</span>

					<span>
						<label for="XXXL">XXXL</label>
						<input type="checkbox" name="size[]" value="XXXL"<?php if ( in_array('XXXL', $size) ) echo ' checked'; ?>>
					</span>

				</div>

				<label for="custom">Custom Options Available</label>
				<br>
				<table>
					<tr>
						<td>
							<select size="15" name="location_left" id="location_left" multiple="multiple">
								<?php $opts = Options::all(); ?>
								@foreach ( $opts as $opt )
									<option value="{{ $opt->id }}">{{ $opt->title }}</option>
								@endforeach
							</select>
						</td>
						<td>
							<button type="button" onclick="move( 'location_right', 'location_left')">&laquo;</button>
							<br><br>
							<button type="button" onclick="move( 'location_left', 'location_right')">&raquo;</button>
						</td>
						<td>
							<select size="15" name="location_right[]" id="location_right" multiple="multiple">
							</select>
						</td>
					</tr>
				</table>

				<div id="upload">
					<div class="box">
						<img src="#" id="preview">
					</div>
					<input style="width:171px;margin-top:10px" name="image" type="file" onchange="readURL(this);" <?php if ( Input::old('image') != '') echo 'value="'.Input::old('image').'"';?>>
				</div>				

			</form>

	</div>
<script type="text/javascript">
	     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(185);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('button.save').click(function() {
        	$('form.addProduct').submit();
        });
</script>
{{ HTML::script('js/customOptions.js') }}
@include('dashboard/_partials/footer')