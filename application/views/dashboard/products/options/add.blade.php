@include('dashboard/_partials/header')

	<div class="full">
			<h2>
				Add New Option
				<div class="right">
					<button onclick="window.location='{{ URL::to('dashboard/products/options') }}'">&laquo; Back</button>
					<button class="save">Save</button>
				</div>
			</h2>
			
			<form method="post" action="{{ URL::to('dashboard/products/options/add') }}" class="addProduct" enctype="multipart/form-data" accept-charset="UTF-8"  onsubmit="return selectAll( new Array( 'location_right' ) );">
				
				<label class="top" for="title">Option Title</label>
				<input type="text" size="60" name="title">
				<br>
				<label class="top" for="price">Price</label>
				<input type="text" size="20" name="price" value="$">

				<br>

				<label class="top" for="customOption">Form Option</label>
				<select name="customOption">
					<option value="0">None</option>
					<option value="1">Text Area</option>
					<option value="2">Input Text</option>
				</select>

				<br>

				<label class="top" for="description">Descripion</label><br>
				<p>
					<textarea name="description" cols="30" rows="10"></textarea>
				</p>
				<input type="submit">
			</form>

	</div>
<script type="text/javascript">
	     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(185);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('button.save').click(function() {
        	$('form.addProduct').submit();
        });
</script>
{{ HTML::script('js/customOptions.js') }}
@include('dashboard/_partials/footer')