@include('dashboard/_partials/header')

	<div class="full">
			<h2>
				Manage {{ $opt->title }}
				<div class="right">
					<button onclick="window.location='{{ URL::to('dashboard/products/options') }}'">&laquo; Back</button>
					<button onclick="window.location='{{ URL::to('dashboard/products/options/delete') }}/{{ $opt->id }}'">Delete</button>
					<button class="save">Update</button>
				</div>
			</h2>
			
			<form method="post" action="{{ URL::to('dashboard/products/options/update') }}" class="addProduct">
				<input type="hidden" name="id" value="{{ $opt->id }}">
				<label class="top" for="title">Option Title</label>
				<input type="text" size="60" name="title" value="{{ $opt->title }}">
<br>
				<label class="top" for="price">Price</label>
				<input type="text" size="20" name="price" value="{{ $opt->price }}">

				<br>
			<label class="top" for="customOption">Form Option</label>
				<select name="customOption">
					<option value="0"<?php if ( $opt->form == '0' ) echo ' selected'; ?>>None</option>
					<option value="1"<?php if ( $opt->form == '1' ) echo ' selected'; ?>>Text Area</option>
					<option value="2"<?php if ( $opt->form == '2' ) echo ' selected'; ?>>Input Text</option>
				</select>
				<br>

				<label class="top" for="description">Descripion</label><br>
				<p>
					<textarea name="description" cols="30" rows="10">{{ $opt->description }}</textarea>
				</p>
				<input type="submit">
			</form>

	</div>
<script type="text/javascript">
	     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(185);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('button.save').click(function() {
        	$('form.addProduct').submit();
        });
</script>
{{ HTML::script('js/customOptions.js') }}
@include('dashboard/_partials/footer')