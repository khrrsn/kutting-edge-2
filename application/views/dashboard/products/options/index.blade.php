@include('dashboard/_partials/header')
	<div class="clients full">
		<h2>
			Organizations
			<div class="right">
				<button onclick="window.location='{{ URL::to('dashboard/products/') }}'">&laquo; Back to Products</button>
				<button onclick="window.location='{{ URL::to('dashboard/products/options/add') }}'">Add Option</button>
			</div>
		</h2>
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
			<?php $opts = Options::all(); ?>
			@foreach ($opts as $opt)
				<tr>
					<td>{{ $opt->title }}</td>
					<td>{{ $opt->description }}</td>
					<td><button onclick="window.location='{{ URL::to('dashboard/products/options/manage/') }}{{ $opt->id }}'">Manage</button></td>
				</tr>
			@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')