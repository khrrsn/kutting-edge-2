@include('dashboard/_partials/header')
	<?php $client = Client::find(Auth::user()->id) ?>
	<div class="clients">
		<h2>
			Your Sales
			<div class="right">Total: <?php $totalSales = CartSession::where('client', '=', $client->id)->count(); ?>
						{{ $totalSales }}</div>
		</h2>
		
		<h3>Clients Customer have Ordered:</h3>
		<table>
			<tr>
				<th>Product</th>
				<th>Price</th>
				<th>Quantity</th>
			</tr>
			<?php $products = unserialize($client->products) ?>
			<?php $q = 0; ?>
			<?php $payment = 0; ?>
			@foreach ( $products as $product )
				<?php $optPrice = 0; ?>
				<?php $product = Product::find($product); ?>
				<tr>
					<th>{{ $product->title }}</th>
					<th>{{ $product->price }}
						<?php $price = explode('$', $product->price); ?>
						<?php $prePrice = $price[1]; ?>
					</th>
					<th>
						<?php $sales = CartSession::where('client', '=', $client->id)->get(); ?>
						@foreach ($sales as $sale)
							<?php $items = CartProducts::where('shopper', '=', $sale->id)->get(); ?>
							@foreach ( $items as $item )
								<?php $item = unserialize($item->product); ?>
								<?php if ( $product->id == $item['product_id'] ) {
									$product_quantity = 1 * $item['quantity'];
									$q = $q + $product_quantity;
									$prePrice = $prePrice * $product_quantity;
									if ( isset($item['options']) ) :
										foreach ($item['options'] as $opt ) :
										$opt = Options::find($opt);
										$tempOptprice = explode('$', $opt->price);
										$optPrice = $optPrice + $tempOptprice[1];
									endforeach;
									endif;
									$payment = $payment + $prePrice;
								} ?>
							@endforeach
						@endforeach
						<?php $payment = $payment + $optPrice; ?>
						{{ $q }}
					</th>
				</tr>
			@endforeach
			<tr>
				
			</tr>
		</table>
		<?php if ( $client->payment == '1' ) : ?>
		<h3>Payments Made:</h3>
		<table>
			<thead>
				<tr>
					<th>Payment Date</th>
					<th>Payment Amount</th>
				</tr>
			</thead>
			<?php $payments = Payment::where('client', '=', $client->id)->get(); $paymentTotal = 0; ?>
			@foreach( $payments as $paid )
				<tr>
					<td>{{ $paid->updated_at }}</td>
					<td>{{ $paid->payment }}</td>
					<?php $paymentTotal = $paymentTotal + $paid->payment; ?>
				</tr>
			@endforeach
		</table>
		<?php $payment = $payment - $paymentTotal; ?>
		<h3>Total Owed: ${{ money_format('%i', $payment) }}</h3>
		<form action="https://checkout.psigate.com/HTMLPost/HTMLMessenger" id="payment" method="POST">
			<input type="hidden" name="StoreKey" value="KuttingEdgedaIbD1Ga012117">
			<input type="hidden" name="CustomerRefNo" value="{{ $client->id }}">
			{{ Form::label('Bname', 'Billing Name') }}
				{{ Form::text('Bname', '', array('placeholder' => 'Your Name (Billing)')) }} <br>
			<input type="hidden" name="Bcompany" value="{{ $client->customername }}">
			{{ Form::label('Baddress1', 'Billing Address') }}
				{{ Form::text('Baddress1') }} <br>
			{{ Form::label('Baddress2', '') }}
				{{ Form::text('Baddress2') }} <br>
			{{ Form::label('Bcity', 'City') }}
				{{ Form::text('Bcity') }} <br>
			{{ Form::label('Bprovince', 'Province') }}
				{{ Form::text('Bprovince') }} <br>
			{{ Form::label('Bpostalcode', 'Postal Code') }}
				{{ Form::text('Bpostalcode') }} <br>
			{{ Form::hidden('Bcountry', 'CA') }}
			{{ Form::label('Phone', 'Phone') }}
				{{ Form::text('Phone') }} <br>
			{{ Form::label('Email', 'Email') }}
				{{ Form::text('Email', $client->email) }} <br>
			{{ Form::label('Payment', 'SubTotal') }}
				{{ Form::text('SubTotal') }}

	<input type="hidden" name="testMode" value="1">
	<input type="hidden" name="ThanksURL" value="{{ URL::to('payment') }}">
	<input type="hidden" name="NoThanksURL" value="{{ URL::to('payment') }}">
	<input type="hidden" name="ResponseFormat" value="HTML">
	<br>
	<button type="submit">Make a Payment!</button>
</form>
<?php endif; ?>

		<table>
			<thead>
				<tr>
					<th>Client Name</th>
					<th>Address</th>
					<th>Phone Number</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
				<?php $sales = CartSession::where('client', '=', $client->id)->get(); ?>
				@foreach ( $sales as $sale)
				<tr>
					<?php $saleInfo = unserialize($sale->info); ?>
					<td>{{ $saleInfo['firstName'] }} {{ $saleInfo['lastName'] }}</td>
					<td><?php if ( isset($saleInfo['address-1']) ) : ?>{{ $saleInfo['address-1'] }}<?php endif; ?></td>
					<td><?php if ( isset($saleInfo['phoneNumber']) ) : ?>{{ $saleInfo['phoneNumber'] }}<?php endif; ?></td>
					<td><a href="{{ URL::to('dashboard/sale/') }}{{ $sale->id }}"><button>View</button</a></td>
				</tr>
				@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')