@include('dashboard/_partials/header')
<?php $customer = unserialize($cart_session->info); ?>
	<div class="clients full">
		<h2>
			Individual Sale for: {{$customer['firstName'] }} {{ $customer['lastName']; }}
			<div class="right">
				<button onClick="window.print();">Print</button>
			</div>
		</h2>
		<table>
			<tr>
				<th>Client Name</th>
				<th>Address</th>
				<th>Phone Number</th>
			</tr>
			<tr>
				<th>{{ $customer['firstName'] }} {{ $customer['lastName']; }}</th>
				<?php if ( isset($customer['address-1'])) : ?><th>{{ $customer['address-1'] }}, {{ $customer['address-2'] }}</th><?php endif; ?>
				<?php if ( isset($customer['phoneNumber'])) : ?><th>{{ $customer['phoneNumber'] }}</th><?php endif; ?>
			</tr>
		</table>

			<table class="products">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Color</th>
							<th>Size</th>
							<th>QTY</th>
							<th>Price</th>
						</tr>
					</thead>
					<?php $products = CartProducts::where('shopper', '=', $cart_session->id)->get(); $sum = 0; ?>
					<?php $sum = 0; ?>
					@foreach( $products as $prod )
						<?php $prod = unserialize($prod->product); ?>
						<?php $product = Product::find($prod['product_id']); ?>
						<?php $color = Color::find($prod['color']); ?>
						<tr>
							<td>{{ $product->title }}</td>
							<td>{{ $color->title }}</td>
							<td>{{ $prod['sizes'] }}</td>
							<td>{{ $prod['quantity'] }}</td>
							<?php $price = explode('$', $product->price) ?>
							<td>${{ money_format('%i', $prod['quantity'] * $price[1]) }}</td>
							<?php $sum += $prod['quantity'] * $price[1]; ?>
						</tr>
					@endforeach
				</table>
				<br>
				<table class="options">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Custom Option/s</th>
							<th>Custom Value/s</th>
							<th>Price</th>
						</tr>
					</thead>
					@foreach ( $products as $prod )
						<?php $options = unserialize($prod->options); ?>
						<?php $prod = unserialize($prod->product); ?>
						<?php $product = Product::find($prod['product_id']); ?>
						<?php if ( isset($prod['options']) ) : ?>
						<tr>
							<td>{{ $product->title }}</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php $opts = Options::find($opt); ?>
									{{ $opts->title }}, 
								@endforeach
							</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php echo $options[$opt]; ?>, 
								@endforeach
							</td>
							<td>
								@foreach( $prod['options'] as $opt )
									<?php $opts = Options::find($opt); ?>
									
									<?php $optprice = explode('$', $opts->price); ?>
									<?php $sum += $prod['quantity'] * $optprice[1]; ?>
									${{ money_format('%i', $optprice[1]) }},
								@endforeach
							</td>
						</tr>
					<?php endif; ?>
					@endforeach

				</table>
	</div>
@include('dashboard/_partials/footer')