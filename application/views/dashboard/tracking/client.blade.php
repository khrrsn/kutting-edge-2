@include('dashboard/_partials/header')
	<div class="clients">
		<h2>
			Sales for {{ $client->customername }}
			<div class="right">Total: <?php $totalSales = CartSession::where('client', '=', $client->id)->count(); ?>
						{{ $totalSales }}</div>
		</h2>
		<h3>Clients Customer have Ordered:</h3>
		<table>
			<tr>
				<th>Product</th>
				<th>Price</th>
				<th>Quantity</th>
			</tr>
			<?php $products = unserialize($client->products) ?>
			@foreach ( $products as $product )
				<?php $product = Product::find($product); ?>
				<tr>
					<th>{{ $product->title }}</th>
					<th>{{ $product->price }}</th>
					<th>
						<?php $q = 0; ?>
						<?php $sales = CartSession::where('client', '=', $client->id)->get(); ?>
						@foreach ($sales as $sale)
							<?php $items = CartProducts::where('shopper', '=', $sale->id)->get(); ?>
							@foreach ( $items as $item )
								<?php $item = unserialize($item->product); ?>
								<?php if ( $product->id == $item['product_id'] ) {
									$product_quantity = 1 * $item['quantity'];
									$q = $q + $product_quantity;
								} ?>
							@endforeach
						@endforeach
						{{ $q }}
					</th>
				</tr>
			@endforeach
			<tr>
				
			</tr>
		</table>
		<h3>Payments:</h3>
		<?php $moneyPaid = 0; ?>
		<?php $payments = Payment::where('client', '=', $client->id)->get(); ?>
		<?php  if ($client->payment == '1') : ?>
			<table>
				<tr>
					<th>Card</th>
					<th>Total</th>
					<th>Date Paid</th>
				</tr>
				@foreach ($payments as $payment)
					<?php $customer = CartSession::find($payment->customer); ?>
					<?php $customer = unserialize($customer->info); ?>
					<?php $details = unserialize($payment->info); ?>

					<tr>
						<td>{{ $details['CardNumber']; }}</td>
						<td>${{ $details['FullTotal'] }}</td>
						<td>{{ $payment->created_at }}</td>
						<?php $moneyPaid = $moneyPaid + $details['FullTotal']; ?>
					</tr>
				@endforeach
			</table>
		<?php elseif ($client->payment == '2') : ?>
			<table>
				<tr>
					<th>Customer Name</th>
					<th>Ref No.</th>
					<th>Card</th>
					<th>Total</th>
					<th>Date Paid</th>
				</tr>
				@foreach ($payments as $payment)
					<?php $customer = CartSession::find($payment->customer); ?>
					<?php $customer = unserialize($customer->info); ?>
					<?php $details = unserialize($payment->info); ?>

					<tr>
						<td>{{ $customer['firstName'] . ' ' . $customer['lastName'] }}</td>
						<td>{{ $payment->customer }}</td>
						<td>{{ $details['CardNumber']; }}</td>
						<td>${{ $details['FullTotal'] }}</td>
						<td>{{ $payment->created_at }}</td>
						<?php $moneyPaid = $moneyPaid + $details['FullTotal']; ?>
					</tr>
				@endforeach
			</table>
		<?php endif; ?>
		<table>
		<h3>${{ money_format('%i', $moneyPaid)}} Paid Out So Far.</h3>
			
		</table>
		<table class="individuals">
			<?php
				$org = Organization::find($client->organization);
				$org = unserialize($org->required);
			?>
			<thead>
				<tr>
					<?php if ( in_array('firstName', $org) ) : ?>
					<th>Client Name</th>
					<?php endif; if ( in_array('address', $org)) : ?>
					<th>Address</th>
					<?php endif; if ( in_array('phoneNumber', $org)) : ?>
					<th>Phone Number</th>
					<?php endif; ?>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
				<?php $sales = CartSession::where('client', '=', $client->id)->get(); ?>
				@foreach ( $sales as $sale)
				<tr>
					<?php $saleInfo = unserialize($sale->info); ?>
					<?php if ( in_array( 'firstName', $org )) : ?>
					<td>{{ $saleInfo['firstName'] }} {{ $saleInfo['lastName'] }}</td>
					<?php endif; if ( in_array('address', $org)) : ?>
					<td>{{ $saleInfo['address-1'] }}</td>
					<?php endif; if ( in_array('phoneNumber', $org)) : ?>
					<td>{{ $saleInfo['phoneNumber'] }}</td>
					<?php endif;?>
					<td><button onclick="window.location='{{ URL::to('dashboard/tracking/client/sale/') }}{{ $sale->id }}'">View</button><button onclick="window.location='{{ URL::to('sale/delete') }}/{{ $sale->id }}'">Delete</button></td>
				</tr>
				@endforeach
		</table>
	</div>
	<script type="text/javascript">
		$(function() {
			$('table.individuals').dataTable();
		});
	</script>
@include('dashboard/_partials/footer')