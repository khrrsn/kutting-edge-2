@include('dashboard/_partials/header')
	<div class="clients">
		<h2>
			Tracking
		</h2>
		<table>
			<thead>
				<tr>
					<th>Client Name</th>
					<th>Address</th>
					<th>Organization</th>
					<th>Order Start</th>
					<th>Order Finish</th>
					<th>Total Sales</th>
					<th class="buttons">&nbsp;</th>
				</tr>
			</thead>
				<?php $clients = Client::all(); ?>
				<tr>
				@foreach ( $clients as $client)
				<td>{{ $client->customername }}</td>
					<td>{{ $client->email }}</td>
					<td><?php 
						$organization = Organization::find($client->organization);
						echo $organization->organization;
					?></td>
					<td>{{ $client->order_start }}</td>
					<td>{{ $client->order_finish }}</td>
					<td>
						<?php $totalSales = CartSession::where('client', '=', $client->id)->count(); ?>
						{{ $totalSales }}
					</td>
				<td><button onclick="window.location='{{ URL::to('dashboard/tracking/client/') }}{{ $client->id }}';">View</button></td>
				</tr>
				@endforeach
		</table>
	</div>
@include('dashboard/_partials/footer')