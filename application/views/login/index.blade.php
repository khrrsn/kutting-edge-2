<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Kutting Edge: Login</title>
	{{ HTML::style('/css/global.css') }}
	{{ HTML::style('/css/login.css') }}
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	{{ HTML::script('js/form.js') }}
</head>
<body>
	<div class="login-box">
		<h1>Kutting Edge Promotions Inc.</h1>
		@if (Session::has('login_errors'))
			<span class="error">Username or password incorrect.</span>
		@endif
		{{ Form::open('login') }}
			
			<div>
				<label for="username">Username</label>
					<input type="text" name="username">
			</div>
			
			<div>
				<label for="password">Password</label>
					<input type="password" name="password">
			</div>

			<button type="submit" class="submit">Submit</button>
			
			<div class="clear"></div>

		{{ Form::close() }}
	</div>
	<script type="text/javascript">
		$(function() {
			$('select').fancyselect();
		});

		$(window).resize(function(){

			$('.login-box').css({
				position:'absolute',
				left: ($(window).width() - $('.login-box').outerWidth())/2,
				top: ($(window).height() - $('.login-box').outerHeight())/2
			});

		});

		// To initially run the function:
		$(window).resize();
	</script>
</body>
</html>