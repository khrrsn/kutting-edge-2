<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

Route::get('/', function()
{
	if ( Auth::user() ) {
		return Redirect::to('dashboard');
	}
	return View::make('login.index');
});
Route::get('login', function()
{
	return View::make('login.index');
});

Route::post('login', function () {
	$userdata = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	if ( Auth::attempt($userdata)) {
		return Redirect::to('dashboard');
	} else {
		return Redirect::to('/')->with('login_errors', true);
	}
});

Route::any('logout', function() {
	Auth::logout();
	return Redirect::to('/');
});

Route::any('dashboard', array('before' => 'auth', function() {
		if ( Auth::user()->role == '1' ) {
			return View::make('dashboard.tracking.index');
		} elseif ( Auth::user()->role == '2' ) {
			return View::make('dashboard.client');
		} else {
			return Redirect::to('login');
		}
}));

Route::any('dashboard/search', array('before' => 'auth', function() {
				$search = Input::get('s');
				$query = '%' . $search . '%';
				$clients = Client::where('customerName', 'LIKE', $query)->get();
				return View::make('dashboard.search')
					->with('clients', $clients)
					->with('search', $search);
			}));

Route::any('dashboard/sale/(:num)', function($id) {
	if ( Auth::user()->role == '2' ) {
		$cart_session = CartSession::find($id);
		return View::make('dashboard.tracking.individual')
			->with('cart_session', $cart_session);
	}
});

Route::group(array('before' => 'admin'), function() {

			Route::any('dashboard/clients', function() {
				return View::make('dashboard.clients.index');
			});

			Route::get('dashboard/clients/add', function() {
				return View::make('dashboard.clients.add');
			});

			Route::post('dashboard/clients/add', function() {

				$input = Input::all();

				$rules = array(
						'customerName'    => 'required|max:75',
						'billingAddress1' => 'required',
						'city'            => 'required',
						'province'        => 'required',
						'postal'          => 'required|size:7',
						'username'        => 'required',
						'password'        => 'confirmed',
						'email'           => 'confirmed',
						'organization'    => 'required',
						'order_start'     => 'required',
						'order_finish'    => 'required',
						'colours'         => 'required',
				);

				$validation = Validator::make($input, $rules);
				if ($validation->fails())
				{
					return Redirect::to('dashboard/clients/add')->with_errors($validation->errors);
				}

				$id = rand(100, 999);
				$client = Client::create(
					array(
						'id'              => $id,
						'customerName'    => Input::get('customerName'),
						'billingAddress1' => Input::get('billingAddress1'),
						'billingAddress2' => Input::get('billingAddress2'),
						'billingAddress3' => Input::get('billingAddress3'),
						'city'            => Input::get('city'),
						'province'        => Input::get('province'),
						'postal'          => Input::get('postal'),
						'username'        => Input::get('username'),
						'password'        => Hash::make(Input::get('password')),
						'email'           => Input::get('email'),
						'organization'    => Input::get('organization'),
						'order_start'     => Input::get('order_start'),
						'order_finish'    => Input::get('order_finish'),
						'colours'         => serialize(Input::get('colours')),
						'products'        => serialize(Input::get('location_right')),
						'shipping'        => Input::get('shipping'),
						'payment'         => Input::get('payment'),
					)
				);
				
				

				$user = User::create(
					array(
						'id'       => $id,
						'name'     => Input::get('customerName'),
						'username' => Input::get('username'),
						'password' => Hash::make(Input::get('password')),
						'role'     => '2',
					)
				);

				if ( $client && $user ) {

					$mailer = IoC::resolve('phpmailer');
						$message = "Congradulations! Your account have been created. <p>Username: " . Input::get('username') . "<br> Password: " . Input::get('password') . "</p><p>Here are the links you will use:</p><p><a href='". URL::to('orders/') . $id ."'>". URL::to('orders/') . $id ." (Order Form)<br><a href='".URL::to('dashboard')."'>".URL::to('dashboard')." (Dashboard)</a></a>";
						try {
					    $mailer->AddAddress( Input::get('email'), Input::get('customerName') );
					    $mailer->Subject  = "Kutting Edge: Your Account has been Created!";
					    $mailer->IsHTML(true);
					    $mailer->Body = View::make('extra/email')->with('client', $client)->with('message', $message);
					    $mailer->Send();
					    $mailer->ClearAllRecipients();
					} catch (Exception $e) {
					    echo 'Message was not sent.';
					    echo 'Mailer error: ' . $e->getMessage();
					}

					return Redirect::to('dashboard/clients');
				} else {
					return 'error';
				}

			});

			Route::post('dashboard/clients/update', function() {
				$id = Input::get('id');
				
				$client = Client::find($id);
				
				$client->customername = Input::get('customerName');
				$client->billingaddress1 = Input::get('billingAddress1');
				$client->billingaddress2 = Input::get('billingAddress2');
				$client->billingaddress3 = Input::get('billingAddress3');
				$client->postal = Input::get('postal');
				$client->city = Input::get('city');
				$client->province = Input::get('province');
				$client->email = Input::get('email');
				$client->order_start = Input::get('order_start');
				$client->order_finish = Input::get('order_finish');
				$client->organization = Input::get('organization');
				$client->shipping = Input::get('shippingcost');
				$client->products = serialize(Input::get('location_right'));
				$client->colours = serialize(Input::get('colours'));
				$client->payment = Input::get('payment');

				$client->save();


				$user = User::find($id);
				$user->name = Input::get('customerName');
				$user->save();

				return Redirect::to('dashboard/clients');
			});

			Route::any('dashboard/clients/organization', function() {
				return View::make('dashboard.clients.organization.index');
			});

			Route::any('dashboard/clients/organization/add', function() {
				return View::make('dashboard.clients.organization.add');
			});

			Route::post('dashboard/clients/organization/create', function() {
				$required = Input::get('required');
				$required = serialize($required);

				$organization = Organization::create(
					array(
						'organization' => Input::get('organization'),
						'required' => $required,
						'custom' => serialize(Input::get('custom')),
					)
				);

				if ($organization) {
					return Redirect::to('dashboard/clients/organization');
				}
			});

			Route::any('dashboard/clients/organization/delete/(:num)', function($id) {
				$org = Organization::find($id);
				if ( $org->delete() ) {
					return Redirect::to('dashboard/clients/organization');
				}
			});

			Route::any('dashboard/clients/organization/manage/(:num)', function($id) {
				$org = Organization::find($id);
				return View::make('dashboard.clients.organization.manage')->with('org', $org);
			});
			Route::post('dashboard/clients/organization/update', function() {
				$id = Input::get('id');
				$org = Organization::find($id);

				$org->organization = Input::get('organization');
				$org->required     = serialize(Input::get('required'));
				$org->custom       = serialize(Input::get('custom'));

				$org->save();

				return Redirect::to('dashboard/clients/organization');
			});

			Route::get('dashboard/clients/colors', function() {
				return View::make('dashboard.clients.colors.index');
			});
			Route::get('dashboard/clients/colors/add', function() {
				return View::make('dashboard.clients.colors.add');
			});
			Route::post('dashboard/clients/colors/add', function() {
				$color = Color::create(
					array(
						'title' => Input::get('title'),
						'hex' => Input::get('color'),
					)
				);
				if ($color) {
				return Redirect::to('dashboard/clients/colors');
				}
			});

			Route::any('dashboard/clients/manage/(:num)', function($client) {
				$client = Client::find($client);
				return View::make('dashboard.clients.manage')->with('client', $client);
			});

			Route::any('dashboard/clients/delete/(:num)', function($id) {
				$client = Client::find($id);
				if ( $client->delete() ) {
					$user = User::find($id);
					if ($user->delete() ) {
						return Redirect::to('dashboard/clients/');
					}
				}
			});

	Route::any('dashboard/products', function() {
		return View::make('dashboard.products.index');
	});

			Route::get('dashboard/products/add', function() {
				return View::make('dashboard.products.add');
			});
			Route::post('dashboard/products/add', function() {

				$input = Input::all();

				$rules = array(
					'productTitle' => 'required',
					'unitPrice' => 'required|min:2',
					'size' => 'required',
					'image'  => 'image'
				);

				$v = Validator::make($input, $rules);

				if ($v->fails())
				{
					return Redirect::to('dashboard/products/add')->with_errors($v->errors)->with_input();
				}

				Bundle::start('resizer');
				$rand = rand(1000, 9999) . '_';
				$upload = Input::upload('image', 'public/uploads/', $rand . Input::file('image.name'));
				$thumb = 'public/uploads/thumb_' . $rand . Input::file('image.name');
				$img = Input::file('image');
				$success = Resizer::open(  $upload )
					->resize(156, 156, 'crop' )
					->save($thumb, 90);
				$file = 'uploads/' . $rand . Input::file('image.name');
				$product = Product::create(
					array(
						'title'   => Input::get('productTitle'),
						'price'   => Input::get('unitPrice'),
						'sizes'   => serialize(Input::get('size')),
						'options' => serialize(Input::get('location_right')),
						'file'    => $file
					)
				);
				if ($upload && $product) {
					return Redirect::to('dashboard/products');
				}
			});
			Route::get('dashboard/products/manage/(:num)', function($id) {
				$product = Product::find($id);
				return View::make('dashboard.products.manage')->with('product', $product);
			});
			Route::post('dashboard/products/manage/(:num)', function($id) {
				$product = Product::find($id);

				if ( Input::file('image.name') != '' ) {
					unlink( 'public/' . $product->file );
					Bundle::start('resizer');
					$rand = rand(1000, 9999) . '_';
				$upload = Input::upload('image', 'public/uploads/', $rand . Input::file('image.name'));
				$thumb = 'public/uploads/thumb_' . $rand . Input::file('image.name');
				$img = Input::file('image');
				$success = Resizer::open(  $upload )
					->resize(156, 156, 'crop' )
					->save($thumb, 90);
				$file = 'uploads/' . $rand . Input::file('image.name');
				$product->file = $file;
				}

				$product->title = Input::get('productTitle');
				$product->price = Input::get('unitPrice');
				$product->sizes = serialize(Input::get('size'));
				$product->options = serialize(Input::get('location_right'));

				if ( $product->save() ) {
					return Redirect::to('dashboard/products');
				}
			}); 

			Route::get('dashboard/products/delete/(:num)', function($id) {
				$product = Product::find($id);
				unlink( 'public/' . $product->file);
				if ( $product->delete() ) {
					return Redirect::to('dashboard/products/');
				}

			});

			Route::get('dashboard/products/options', function() {
				return View::make('dashboard.products.options.index');
			});

			Route::get('dashboard/products/options/add', function() {
				return View::make('dashboard.products.options.add');
			});
			Route::post('dashboard/products/options/add', function() {
				$option = Options::create(
					array(
						'title' => Input::get('title'),
						'price' => Input::get('price'),
						'form' => Input::get('customOption'),
						'description' => Input::get('description')
					)
				);

				if ($option) {
					return Redirect::to('dashboard/products/options');
				}
			});

			Route::any('dashboard/products/options/manage/(:num)', function($id) {
				$opt = Options::find($id);
				return View::make('dashboard.products.options.manage')->with('opt', $opt);
			});

			Route::any('dashboard/products/options/delete/(:num)', function($id) {
				$opt = Options::find($id);
				if ($opt->delete() ) {
					return Redirect::to('dashboard/products/options/');
				}
			});

			Route::post('dashboard/products/options/update', function() {
				$id = Input::get('id');
				$opt = Options::find($id);

				$opt->title = Input::get('title');
				$opt->price = Input::get('price');
				$opt->form  = Input::get('customOption'); 
				$opt->description = Input::get('description');

				if ( $opt->save() ) {
					return Redirect::to('dashboard/products/options');
				}
			});



	Route::any('dashboard/tracking', function() {
		return View::make('dashboard.tracking.index');
	});

	Route::any('dashboard/tracking/client/(:num)', function($id) {
		$client = Client::find($id);
		return View::make('dashboard.tracking.client')
			->with('client', $client);
	});
	Route::any('dashboard/tracking/client/sale/(:num)', function($id) {
		$cart_session = CartSession::find($id);
		return View::make('dashboard.tracking.individual')
			->with('cart_session', $cart_session);
	});
	Route::any('sale/delete/(:num)', function($id) {
		$cart_session = CartSession::find($id);
		if ( $cart_session->delete() ) {
			return Redirect::to('dashboard/tracking');
		}		
	});

});

Route::any('orders/(:num)', function($id) {
	$client = Client::find($id);
	return View::make('orders.index')->with('client', $client);
});
Route::post('orders/items', function() {
	$session_id = rand(100000, 999999);
	$cart_session = CartSession::create(
		array(
			'id' => $session_id,
			'client' => Input::get('id'),
			'info' => serialize(Input::all()),
		) 
	);
	$id = Input::get('id');
	$client = Client::find($id);
	return View::make('orders.items')
		->with('client', $client)
		->with('session_id', $session_id);
});

Route::post('orders/add/(:num)', function($session) {
	$cart_product = CartProducts::create(
		array(
			'product' => serialize(Input::all()),
			'shopper' => $session,
			'options' => serialize(Input::get('formOption')),
		)
	);
});
Route::post('orders/finalization', function() {
	$session_id = Input::get('session_id');

	$finalStep = CartSession::find($session_id);
	$finalStep->save();

	$id = Input::get('client');
	$client = Client::find($id);
	return View::make('orders.finalization')
		->with('client', $client)
		->with('session_id', $session_id);
});

Route::post('orders/confirm', function() {
	$cart = CartSession::find(Input::get('session_id'));
	$cart->confirmed = '1';
	$cart->shipping = Input::get('shipping');
	if ( $cart->save() ) {
		$client = Client::find(Input::get('client'));

		$mailer = IoC::resolve('phpmailer');
		$name = $cart->firstName . ' ' . $cart->lastName;
		$cart = CartSession::find(Input::get('session_id'));
		$info = unserialize($cart->info);
		$name = $info['firstName'] . ' ' . $info['lastName'];
		try {
		    $mailer->AddAddress( $info['email'], $name );
		    $mailer->Subject  = "Kutting Edge: Your Order is Confirmed";
		    $mailer->IsHTML(true);
		    $mailer->Body = View::make('extra/order_email')->with('cart', $cart);
		    $mailer->Send();
		} catch (Exception $e) {
		    echo 'Message was not sent.';
		    echo 'Mailer error: ' . $e->getMessage();
		}

		return View::make('orders/thanks')->with('client', $client);

	}
});


Route::any('orders/cancel/(:num)', function($id) {
	$cart_session = CartSession::find($id);
	$client_id = $cart_session->client;
	if ( $cart_session->delete() ) {
		CartProducts::where('shopper', '=', $id)->delete();
		$redirectURL = 'orders/' . $client_id;
		return Redirect::to($redirectURL);
	}
});


Route::any('mailTest', function() {
	$mailer = IoC::resolve('phpmailer');

	try {
    $mailer->AddAddress( 'khninethree@gmail.com', 'Keith Harrison' );
    $mailer->Subject  = "Laravel Rocks";
    $mailer->IsHTML(true);
    $mailer->Body = View::make('emailtest');
    $mailer->Send();
} catch (Exception $e) {
    echo 'Message was not sent.';
    echo 'Mailer error: ' . $e->getMessage();
}
});

Route::any('kutting_edge_cron_jobs', function() {

	$clients = Client::all();

	return View::make('extra/cronjobs')->with('clients', $clients);

});

Route::post('orders/remove', function() {
	$cart_product = CartProducts::find(Input::get('id'));
	$cart_product->delete();
});

Route::get('orders/updateTotal/(:num)', function($id) {
	$session_id = $id;
	return View::make('orders/updatetotal')->with('session_id', $session_id);
});
Route::get('orders/updateTotal/(:num)/(:num)', function($id, $num) {
	$session_id = $id;
	return View::make('orders/updatetotal')->with('session_id', $session_id)->with('num', $num);
});

Route::get('payment', array('before' => 'auth', function() {
	return View::make('dashboard.payment');
}));
Route::get('order/payment', function() {
	$client = array();
	return View::make('orders/payment_status')->with('client', $client);
});
Route::post('orders/payment', function() {
	$cart = CartSession::find(Input::get('session_id'));
	$cart->confirmed = '1';
	$cart->shipping = Input::get('shipping');
	if ( $cart->save() ) {
		$client = Client::find(Input::get('client'));

		return View::make('orders/payment')->with('client', $client)->with('cart', $cart);

	}
});

Route::get('orders/payment/(:num)', function($id) {
	$cart = CartSession::find($id);
	$client = Client::find($cart->client);
	return View::make('orders/payment')->with('client', $client)->with('cart', $cart);
});

Route::get('orders/thanks/(:num)', function($id) {
	$cart = CartSession::find($id);
		$client = Client::find($cart->client);

		$mailer = IoC::resolve('phpmailer');
		$name = $cart->firstName . ' ' . $cart->lastName;
		$info = unserialize($cart->info);
		$name = $info['firstName'] . ' ' . $info['lastName'];
		try {
		    $mailer->AddAddress( $info['email'], $name );
		    $mailer->Subject  = "Kutting Edge: Your Order is Confirmed";
		    $mailer->IsHTML(true);
		    $mailer->Body = View::make('extra/order_email')->with('cart', $cart);
		    $mailer->Send();
		} catch (Exception $e) {
		    echo 'Message was not sent.';
		    echo 'Mailer error: ' . $e->getMessage();
		}

		return View::make('orders/thanks')->with('client', $client);
});
/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});

Route::filter('admin', function() {
	if ( Auth::guest() ) {
		return Redirect::to('login');
	} elseif (Auth::user()->role != '1') return Redirect::to('dashboard');
});