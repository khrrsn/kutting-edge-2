<?php

class Create_Colors_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('colors', function($table) {
			$table->increments('id');
			$table->string('title', 50);
			$table->string('hex', 7);
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('colors');
	}

}