<?php

class Create_Cart_Session {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cart_session', function($table) {
			$table->increments('id');
			$table->text('info');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cart_session');
	}

}