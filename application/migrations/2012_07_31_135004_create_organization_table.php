<?php

class Create_Organization_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizations', function($table) {
			$table->increments('id');
			$table->string('organization', 155);
			$table->text('required');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizations');
	}

}