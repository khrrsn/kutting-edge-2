<?php

class Create_Payment_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function($table) {
			$table->increments('id');
			$table->integer('payment');
			$table->integer('client');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}