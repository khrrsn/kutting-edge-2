<?php

class Create_Cart_Products {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cart_products', function($table) {
			$table->increments('id');
			$table->text('product');
			$table->integer('shopper');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cart_products');
	}

}