<?php

class Create_Clients {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function($table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('billingAddress1', 128);
			$table->string('billingAddress2', 128);
			$table->string('billingAddress3', 128);
			$table->string('city', 128);
			$table->string('province', 128);
			$table->integer('postal');
			$table->string('username', 64);
			$table->string('password', 64);
			$table->string('email', 128);
			$table->integer('organization');
			$table->date('order_start');
			$table->date('order_finish');
			$table->string('colours', 256);
			$table->text('products');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}